<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>List all collaborating writers for ${currentDraft.title}</title>
<script type="text/javascript">
	function openDraft(details,draft_id,user_id)
	{
		var height = 800;
		var width = 800;
		var ClientWidth = document.documentElement.clientWidth;
		var ClientHeight = document.documentElement.clientHeight;
		var left = (ClientWidth - width) / 2;
		var top = (ClientHeight - height) / 2;
		window.open("./draftviewer?details="+details+"&draft="+draft_id+"&user="+user_id, "draftViewer", "height=" + height + ",width=" + width + ",location=no ,menubar=no,left=" + left + ",top=" + top + " ");
	}
</script>
</head>
<body>
<div>
	<Table border="1">
		<tr>
			<td><b>Writer Name</b></td>
			<td><b>Last edited at</b></td>
			<td><b>Contributions  (click to view the progress)</b></td>
		</tr>
		<c:forEach items="${collaboratedDrafts}" var="draft" varStatus="status">
		<tr>	
			<td><label>${writers[status.index].firstName} ${writers[status.index].lastName}</label></td>
			<td><label>${draft.lastEditTime}</label></td>
			<td><label onclick="openDraft('full','${draft.id}','${writers[status.index].id}')" style="background-color:#DCDCDC">${draft.title}</label></td>
			<c:if test="${currentDraft.mainAuthorId == userId}"><td><a href="./removecollaborator?draft=${draft.id}&user=${writers[status.index].id}&maindraft=${currentDraft.id}">remove collaborator</a></td></c:if>
		</tr>
		</c:forEach>
	</Table>
</div>
</body>
</html>