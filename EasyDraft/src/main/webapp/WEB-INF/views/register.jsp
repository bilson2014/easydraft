<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>EasyDraft - Welcome to register!</title>
<script type="text/javascript">
	    $(document).ready(function() {
	 
	      toggleSubmitButton(); // show/hide box on page load
	 
	      $('#confirmationCheckBox').change(function() { toggleSubmitButton();});
	 
	    });
	 
	    function toggleSubmitButton() {
	      if(!$('#confirmationCheckBox').is('Checked')) {
	        $('#submitButton').prop('disabled', true);
	      } else {
	        $('#submitButton').prop('disabled', false);
	      }
	    }
</script>
</head>
<body>
	Please provide your information and click the submit button:
	
	<label id="alert"><font color="red">${alert}</font></label><br/><br/>
	
	<form:form commandName="user" method="POST" name="register">

	<table>
	<tr>
		<td><label>Login Name*: </label></td>
		<td><form:input path="showName" value="${showName}"/></td>
		<font color="red"><form:errors path="showName"/></font><br/><br/>
	</tr>	
	<tr>
		<td><label>Login Password*: </label></td> 
		<td><form:password path="password" value="${password}"/></td>
		<font color="red"><form:errors path="password"/></font><br/><br/>
	</tr>	
	<tr>	
		<br/>
		<br/>
	</tr>	
	<tr>	
		<td><label>First Name: </label></td> 
		<td><form:input path="firstName" value="${firstName}"/></td>
		<font color="red"><form:errors path="firstName"/></font><br/><br/>
	</tr>	
	<tr>	
		<td><label>Last Name: </label></td> 
		<td><form:input path="lastName" value="${lastName}"/></td>
		<font color="red"><form:errors path="lastName"/></font><br/><br/>
	</tr>	
	<tr>	
		<td><label>Email: </label></td> 
		<td><form:input path="email" value="${email}"/></td>
		<font color="red"><form:errors path="email"/></font><br/><br/>
	</tr>	
	<tr>	
		<td><label>First line of your location: </label></td> 
		<td><form:input path="locationLine1" value="${locationLine1}"/></td>
		<font color="red"><form:errors path="locationLine1"/></font><br/><br/>
	</tr>	
	<tr>	
		<td><label>Second line of your location: </label></td> 
		<td><form:input path="locationLine2" value="${locationLine2}"/></td>
		<font color="red"><form:errors path="locationLine2"/></font><br/><br/>
	</tr>	
	<tr>	
		<td><label>City: </label></td> 
		<td><form:input path="city" value="${city}"/></td>
		<font color="red"><form:errors path="city"/></font><br/><br/>
	</tr>	
	<tr>  
		<td><label>Select your country: </label></td> 
		<td><form:select path="country">
	        <form:option value="">Select a country</form:option>
	        <c:forEach items="${countries}" var="country">
	          <form:option value="${country}">${country}</form:option>
	        </c:forEach>
	     </form:select></td>
		<font color="red"><form:errors path="country"/></font><br/><br/>
	</tr>	
	<tr>
		<td><label>Post code: </label></td> 
		<td><form:input path="postCode" value="${postCode}"/></td>
		<font color="red"><form:errors path="postCode"/></font><br/><br/>
	</tr>	
	<tr>	
		<td><label>Your history (your achievements, master pieces and etc): </label></td> 
		<td><form:textarea path="history" value="${history}" rows="15" cols="150"/></td>
		<font color="red"><form:errors path="history"/></font><br/><br/>
	</tr>	
	<tr>
		<br/>
		<br/>
	</tr>	
	<tr>	
		<td><form:checkbox path="confirmation" id="confirmationCheckBox" /></td>
		<td><label>I have read through the terms and conditions. I confirm I will follow the rules defined when I use EasyDraft.</label></td> 
		 <font color="red"><form:errors path="confirmation"/></font><br/><br/>
	</tr>	
	<tr>
		<br/>
		<br/>
	</tr>	
	<tr>	
		<td><input type="submit" id="submitButton" value="submit"/></td>
	</tr>	
	</table>

	</form:form>
</body>
</html>