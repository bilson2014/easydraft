<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<title>EasyDraft - Draft your ideas at any time in any places.</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script type="text/javascript">
	function listAllCollaborators(draft_id,user_id)
	{
		var height = 600;
		var width = 1000;
		var ClientWidth = document.documentElement.clientWidth;
		var ClientHeight = document.documentElement.clientHeight;
		var left = (ClientWidth - width) / 2;
		var top = (ClientHeight - height) / 2;
		window.open("./listcollaborators?draft="+draft_id+"&user="+user_id, "listcollaborators", "height=" + height + ",width=" + width + ",location=no ,menubar=no,scrollbars=yes,left=" + left + ",top=" + top + " ");
	}
	function addNewWriter(draft_id, user_id)
	{
		var height = 600;
		var width = 400;
		var ClientWidth = document.documentElement.clientWidth;
		var ClientHeight = document.documentElement.clientHeight;
		var left = (ClientWidth - width) / 2;
		var top = (ClientHeight - height) / 2;
		window.open("./selectwriter?draft="+draft_id+"&user="+user_id, "addnewwriter", "height=" + height + ",width=" + width + ",location=no ,menubar=no,scrollbars=yes,left=" + left + ",top=" + top + " ");
	}
	function removeSelf(draft_id, user_id)
	{
	    self.location = "./removecollaborator?draft="+draft_id+"&user="+user_id;
	}
	function productionize(draft_id, user_id)
	{
		var height = 1000;
		var width = 1000;
		var ClientWidth = document.documentElement.clientWidth;
		var ClientHeight = document.documentElement.clientHeight;
		var left = (ClientWidth - width) / 2;
		var top = (ClientHeight - height) / 2;
		window.open("./productionize?draft="+draft_id,"production", "height=" + height + ",width=" + width + ",location=no,menubar=no,scrollbars=yes,left=" + left + ",top=" + top + " ");
	}
	</script>
</head>
<body>
<h1>
	Welcome back ${user.firstName} ! 
</h1>

<div>
  Your locale is ${locale}. 
</div>

<br/>
<br/>

<a href="<c:url value="/drafteditor?action=new" />">Create a new draft</a>
<br/>
<br/>

<div>
	<b><font style="font-family:arial;color:black;font-size:30px;">Your draft List</font></b>
	<Table border="1">
		<tr>
			<td><b>Title</b></td>
			<td><b>Created at</b></td>
			<td><b>Last edited at</b></td>
			<td><b>Collaborating Writers</b></td>
		</tr>
		 <c:forEach items="${selfCreatedDrafts}" var="draft">
		 	<tr>
	          <td><a href="<c:url value="/drafteditor?action=update&draft=${draft.id}" />">${draft.title}</a></td>
	          <td><label>${draft.createTime}</label></td>
	          <td><label>${draft.lastEditTime}</label></td>
	          <td>
	          	<label onclick="listAllCollaborators('${draft.id}', '${user.id}');" style="background-color:#DCDCDC">List all writers</label> 
	          	<label onclick="addNewWriter('${draft.id}', '${user.id}');" style="background-color:#DCDCDC">Add new</label>
	          </td>
	          <td><a href="<c:url value="/drafteditor?action=update&draft=${draft.id}" />">Edit</a></td>
	           <td><a href="<c:url value="/drafteditor?action=download&draft=${draft.id}" />">Download</a></td>
	          <td><a href="<c:url value="/drafteditor?action=delete&draft=${draft.id}" />">Delete</a></td>
	          <td><a onclick="productionize('${draft.id}', '${user.id}');" href="#">Go Production</a></td>
	     	</tr>
	     </c:forEach>
	</Table>
</div>

<br/>
<br/>

<div>
	<b><font style="font-family:arial;color:grey;font-size:30px;">Your participation</font></b>
	<Table border="1">
		<tr>
			<td><b>Title</b></td>
			<td><b>Create at</b></td>
			<td><b>Last edit at</b></td>
			<td><b>Collaborating Writers</b></td>
		</tr>
		<c:forEach items="${coAuthoredDrafts}" var="draft">
		 	<tr>
	          <td><a href="<c:url value="/drafteditor?action=update&draft=${draft.id}" />">${draft.title}</a></td>
	          <td><label>${draft.createTime}</label></td>
	          <td><label>${draft.lastEditTime}</label></td>
	          <td>
	          	<label onclick="listAllCollaborators('${draft.id}', '${user.id}');" style="background-color:#DCDCDC">List all writers</label> 
	          	<label onclick="removeSelf('${draft.id}', '${user.id}');" style="background-color:#DCDCDC">Quit Collaboration</label>
	          </td>
	          <td><a href="<c:url value="/drafteditor?action=update&draft=${draft.id}" />">Edit</a></td>
	           <td><a href="<c:url value="/drafteditor?action=download&draft=${draft.id}" />">Download</a></td>
	          <td><a href="<c:url value="/drafteditor?action=delete&draft=${draft.id}" />">Delete</a></td>
	     	</tr>
	     </c:forEach>
	</Table>
</div>

</body>
</html>
