<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Screenplay Template Editor</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/ueditor/ueditor.all.js"></script>
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/ueditor/jquery-1.3.2.min.js"></script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/ueditor/lang/zh-cn/zh-cn.js"></script> 
	<script type="text/javascript" charset="utf-8">
		$.ajax({
			type : 'GET',
			contentType : 'application/json',
			dataType : 'json',
			url : "<%=request.getContextPath() %>/loadScreenplayTemplates",
			success:function(data){
				var options = {};
				options.templates = data.templates;
				options.initialContent = '${contents}';
				UE.getEditor('editor',options);
			},
			error:function(data){
				alert('Can not fetch Templates');
			}
		});
	</script>
    <style type="text/css">
        .clear {
            clear: both;
        }
    </style>
</head>
<body>
<div>
	<form:form commandName="screenplaytemplate" method="POST" name="screenplaytemplate">
    	Title: <form:input path="title" size="35" value="${title}" style="height:25px;font-size:25px;"></form:input> <br/>
    	Cover Photo: <form:input path="imageURL" size="35" value="${imageURL}" style="height:25px;font-size:25px;"></form:input> <br/>
    	Cover Text: <form:input path="preContents" size="35" value="${preContents}" style="height:25px;font-size:25px;"></form:input> <br/>
    	<span style="padding-left:20px"> <input type="submit" id="submitButton" name="save" value="save"/> </span>
    	<span style="padding-left:20px"> <input type="submit" id="submitButton" name="close" value="close"/> </span> <br/><br/>
    	<span style="padding-left:40px"> &ldquo;<label id="entertainmentProfessionalName">${ep.showName}</label>&rdquo; 
    	created at  &ldquo;<label id="createTime">${screenplaytemplate.createTime} ;</label>&rdquo; </span> <br/><br/>
    	
       	<script id="editor" type="text/plain" name="myContent" style="width:1024px;height:500px;">${contents}</script>
    </form:form>
</div>
<div id="btns">
    <div>
        <button onclick="getAllHtml()">获得整个html的内容</button>
        <button onclick="getContent()">获得内容</button>
        <button onclick="setContent()">写入内容</button>
        <button onclick="setContent(true)">追加内容</button>
        <button onclick="getContentTxt()">获得纯文本</button>
        <button onclick="getPlainTxt()">获得带格式的纯文本</button>
        <button onclick="hasContent()">判断是否有内容</button>
        <button onclick="setFocus()">使编辑器获得焦点</button>
        <button onmousedown="isFocus(event)" style="color:red">new 编辑器是否获得焦点</button>
        <button onmousedown="setblur(event)" style="color:red">new 编辑器失去焦点</button>

    </div>
    <div>
        <button onclick="getText()">获得当前选中的文本</button>
        <button onclick="insertHtml()">插入给定的内容</button>
        <button id="enable" onclick="setEnabled()">可以编辑</button>
        <button onclick="setDisabled()">不可编辑</button>
        <button onclick=" UE.getEditor('editor').setHide()">隐藏编辑器</button>
        <button onclick=" UE.getEditor('editor').setShow()">显示编辑器</button>
        <button onclick=" UE.getEditor('editor').setHeight(300)">设置编辑器的高度为300</button>
    </div>

    <div>
        <button onclick="getLocalData()" style="color:red">new 获取草稿箱内容</button>
        <button onclick="clearLocalData()" style="color:red">new 清空草稿箱</button>
    </div>

</div>
<div>
    <button onclick="createEditor()"/>
    创建编辑器</button>
    <button onclick="deleteEditor()"/>
    删除编辑器</button>
</div>

</body>
<script type="text/javascript">

    function isFocus(e){
        alert(ue.isFocus());
        UE.dom.domUtils.preventDefault(e)
    }
    function setblur(e){
        ue.blur();
        UE.dom.domUtils.preventDefault(e)
    }
    function insertHtml() {
        var value = prompt('插入html代码', '');
        ue.execCommand('insertHtml', value)
    }
    function createEditor() {
        enableBtn();
        UE.getEditor('editor');
    }
    function getAllHtml() {
        alert(UE.getEditor('editor').getAllHtml())
    }
    function getContent() {
        var arr = [];
        arr.push("使用editor.getContent()方法可以获得编辑器的内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getContent());
        alert(arr.join("\n"));
    }
    function getPlainTxt() {
        var arr = [];
        arr.push("使用editor.getPlainTxt()方法可以获得编辑器的带格式的纯文本内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getPlainTxt());
        alert(arr.join('\n'))
    }
    function setContent(isAppendTo) {
        var arr = [];
        arr.push("使用editor.setContent('欢迎使用ueditor')方法可以设置编辑器的内容");
        UE.getEditor('editor').setContent('欢迎使用ueditor', isAppendTo);
        alert(arr.join("\n"));
    }
    function setDisabled() {
        UE.getEditor('editor').setDisabled('fullscreen');
        disableBtn("enable");
    }

    function setEnabled() {
        UE.getEditor('editor').setEnabled();
        enableBtn();
    }

    function getText() {
        //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
        var range = UE.getEditor('editor').selection.getRange();
        range.select();
        var txt = UE.getEditor('editor').selection.getText();
        alert(txt)
    }

    function getContentTxt() {
        var arr = [];
        arr.push("使用editor.getContentTxt()方法可以获得编辑器的纯文本内容");
        arr.push("编辑器的纯文本内容为：");
        arr.push(UE.getEditor('editor').getContentTxt());
        alert(arr.join("\n"));
    }
    function hasContent() {
        var arr = [];
        arr.push("使用editor.hasContents()方法判断编辑器里是否有内容");
        arr.push("判断结果为：");
        arr.push(UE.getEditor('editor').hasContents());
        alert(arr.join("\n"));
    }
    function setFocus() {
        UE.getEditor('editor').focus();
    }
    function deleteEditor() {
        disableBtn();
        UE.getEditor('editor').destroy();
    }
    function disableBtn(str) {
        var div = document.getElementById('btns');
        var btns = domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            if (btn.id == str) {
                domUtils.removeAttributes(btn, ["disabled"]);
            } else {
                btn.setAttribute("disabled", "true");
            }
        }
    }
    function enableBtn() {
        var div = document.getElementById('btns');
        var btns = domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            domUtils.removeAttributes(btn, ["disabled"]);
        }
    }

    function getLocalData () {
        alert(ue.execCommand( "getlocaldata" ));
    }

    function clearLocalData () {
        ue.execCommand( "clearlocaldata" );
        alert("已清空草稿箱")
    }

</script>
</html>