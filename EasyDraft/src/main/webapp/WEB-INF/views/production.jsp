<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Production page for draft ${currentDraft.title}</title>
</head>
<body>
<div>
	<h1>Production Page</h1>
	<form:form commandName="production" method="POST" name="production">
		<h2>Log Line</h2>
		<strong>Please write one sentence to describe your screenplay.</strong> <br/>
		<i>Ideally it includes the main character, the main antagonist, the nature of their conflict, genre elements (if it's a comedy, it should be humorous; if it's a thriller, it should thrill; if horror, it should horrify, etc.), and hints at least of the plot through line (the beginning, middle and end). </i><br/>
		<form:input path="logline" size="100" value="${logline}"></form:input> 
		<h2>Pitch</h2>
		<strong>Please write a teaser pitch for your screenplay. (100 words)</strong> <br/>
		<i>Generally, you get three sentences to hook listeners into the premise, the genre, and the scope of your film. As the rule of thumb, your first sentence introduces the characters, the next sentence illustrates their conflict, and the final sentence leaves listeners wanting more. The conflict generally suggests the film's genre, but if not, consider alluding to that in the final sentence as well. </i><br/>
		<form:textarea path="pitch" size="35" value="${pitch}" rows="10" cols="60" ></form:textarea> <br/>
		<input type="submit" id="submitButton" name="save" value="save"/>
		<h2>Genres</h2>
		<strong>Please select the genres of your screenplay. </strong>
		<form:select path="genre">
	        <c:forEach items="${genres}" var="genre">
	          <form:option value="${genre}">${genre}</form:option>
	        </c:forEach>
	     </form:select>
	</form:form>
	<h2>Previews</h2>
	<c:if test="${not empty exposedPreview}">
		<i>The current publicly exposed preview is <strong>${exposedPreview.previewName} </strong></i><br/><br/>
	</c:if>
	You can create more than one previews of your screen play and send them to producers, directors and even the public. It effectively help you protect your copyright.<br/>
	<a href="<c:url value="/previeweditor?action=new&draft=${currentDraft.id}" />">Create a new preview version</a> <br/><br/>
	<Table border="1">
		<tr>
			<td><b>Preview Name</b></td>
			<td><b>Edit the preview</b></td>
			<td><b>View the preview effect</b></td>
			<td><b>Delete the preview</b></td>
			<td><b>Expose the preview</b></td>
		</tr>
		<c:forEach items="${previews}" var="preview">
		<tr>
			<td><label>${preview.previewName}</label></td>
			<td><a href="<c:url value="/previeweditor?action=update&draft=${currentDraft.id}&preview=${preview.id}" />">Edit</a></td>
			<td><a href="<c:url value="/previeweditor?action=view&draft=${currentDraft.id}&preview=${preview.id}" />">View</a></td>
			<td><a href="<c:url value="/previeweditor?action=delete&draft=${currentDraft.id}&preview=${preview.id}" />">Delete</a></td>
			<td><a href="<c:url value="/previeweditor?action=set&draft=${currentDraft.id}&preview=${preview.id}" />">Set</a></td>
		</tr>
		</c:forEach>
	</Table>
</div>
</body>
</html>