<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Please choose a screen writer to collaborate</title>
</head>
<body>
<div>
	<table border="1">
		<tr>
			<td><b>Writer Name</b></td>
			<td><b>Email</b></td>
		</tr>
		<c:forEach items="${otherWriters}" var="writer">
		<tr>
			<td><a href = "./profile?user=${writer.id}">${writer.firstName} ${writer.lastName}</a></td>
			<td><a href = "mailto:${writer.email}">${writer.email}</a></td>
			<td><a href = "./addnewcollaborator?draft=${draft_id}&user=${writer.id}&origin=${originalWriter}">Add as a contributor</a></td>
		</tr>
		</c:forEach>
	</table>
</div>
</body>
</html>