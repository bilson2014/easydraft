<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>EasyDraft - Let your ideas fly!</title>
</head>
<body>
	<h1>Please Login: </h1>
	
	<form:form commandName="user" method="POST" name="login">

		Username:<form:input path="showName" value="${showName}"/>
		<font color="red"><form:errors path="showName"/></font><br/><br/>

		Password:<form:password path="password" value="${password}"/>
		<font color="red"><form:errors path="password"/></font><br/><br/>

		<label id="alert"><font color="red">${alert}</font></label><br/><br/>
		
		<input type="radio" id="writer" name="identity" value="writer" checked="checked"/> writer<br/>
		<input type="radio" id="entertainmentprofessional" name="identity" value="entertainmentprofessional"/> entertainment professional<br/>
		
		
		<input type="submit" value="Login"/>

	</form:form>
	
	<a href="./createAccount"> Create an account now ! </a>
</body>
</html>