<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Screenspirit - Find your screenplay at anytime and anywhere!</title>
</head>
<body>
<h1>
	Welcome back ${ep.firstName} ! 
</h1>

<div>
  Your locale is ${locale}. 
</div>

<br/>
<br/>

<div>
	<h2>Search Panel</h2>
	<form name="screenplaySearch" method="POST">
		Please give any information about the screenplay you want and try search: <br/><br/>
		<input type="text" name="searchbox" size="50"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="submit" value="search"/> <br/><br/>
	</form>
</div>
<c:if test="${not empty result}">
	<b><font style="color:red">Add Search Result : ${result}</font></b>
</c:if>

<c:if test="${not empty searchResult}">
	<div>
		<b><font style="font-family:arial;color:black;font-size:30px;">Search Results</font></b>
		<Table border="1">
			<tr>
				<td><b>Title</b></td>
				<td><b>Main Author</b></td>
				<td><b>Created At</b></td>
				<td><b>Genre</b></td>
				<td><b>Log Line</b></td>
				<td><b>Add to Interest</b></td>
			</tr>
			<c:forEach items="${searchResult}" var="screenplay" >
			<tr>
				<td><label>${screenplay.title}</label></td>
				<td><label>${screenplay.mainAuthorName}</label></td>
				<td><label>${screenplay.createTime}</label></td>
				<td><label>${screenplay.production.genre}</label></td>
				<td><label>${screenplay.production.logline}</label></td>
				<td><a href="<c:url value="/draftexchange?action=add&author=${screenplay.mainAuthorId}&draft=${screenplay.id}"/>">Add</a></td>
			</tr>
			</c:forEach>			
		</Table>
		<br/>
		<br/>
	</div>
</c:if>


<div>
	<b><font style="font-family:arial;color:blue;font-size:30px;">The screenplays you are interested in</font></b>
	<Table border="1">
		<tr>
			<td><b>Title</b></td>
			<td><b>Main Author</b></td>
			<td><b>Genre</b></td>
			<td><b>Log Line</b></td>
			<td><b>Level of Interest</b></td>
			<td><b>Purchase</b></td>
			<td><b>Remove</b></td>
		</tr>
		<c:forEach items="${interestList}" var="interestDraftExchange" varStatus="status">
		<tr>
			<td><a href="<c:url value="/draftexchange?action=view&draftexchange=${interestDraftExchange.id}"/>">${interestDrafts[status.index].title}</a></td>
			<td><label>${interestAuthor[status.index].firstName} &nbsp; ${interestAuthor[status.index].lastName}</label></td>
			<td><label>${interestDrafts[status.index].production.genre}</label></td>
			<td><label>${interestDrafts[status.index].production.logline}</label></td>
			<td>
				<label>${interestDraftExchange.levelOfInterest}</label>&nbsp;
				<a href="<c:url value="/draftexchange?action=increaseinterestlevel&draftexchange=${interestDraftExchange.id}"/>">+</a>&nbsp;
				<a href="<c:url value="/draftexchange?action=decreaseinterestlevel&draftexchange=${interestDraftExchange.id}"/>">-</a>&nbsp;
			</td>
			<td><a href="<c:url value="/draftexchange?action=buy&draftexchange=${interestDraftExchange.id}"/>">buy</a></td>
			<td><a href="<c:url value="/draftexchange?action=delete&draftexchange=${interestDraftExchange.id}"/>">remove</a></td>
		</tr>
		</c:forEach>
	</Table>
</div>	

<br/>
<br/>

<div>
	<b><font style="font-family:arial;color:green;font-size:30px;">The screenplays you have purchased</font></b>
	<Table border="1">
	<tr>
			<td><b>Title</b></td>
			<td><b>Main Editor</b></td>
			<td><b>Genre</b></td>
			<td><b>Log Line</b></td>
	</tr>
	<c:forEach items="${purchasedList}" var="purchasedDraftExchange" varStatus="status">
	<tr>
		<td><a href="<c:url value="/draftexchange?action=view&draftexchange=${purchasedDraftExchange.id}"/>">${purchasedDrafts[status.index].title}</a></td>
		<td><label>${purchasedAuthor[status.index].firstName} &nbsp; ${purchasedAuthor[status.index].lastName}</label></td>
		<td><label>${purchasedDrafts[status.index].production.genre}</label></td>
		<td><label>${purchasedDrafts[status.index].production.logline}</label></td>
	</tr>
	</c:forEach>
	</Table>
</div>	

<br/>
<br/>

<div>
	<b><font style="font-family:arial;color:grey;font-size:30px;">Manage your Screenplay Templates</font></b> <br/>
	<a href="<c:url value="/screenplaytemplate?action=new"/>">Click here to create a New Template</a>
	<Table border="1">
	<tr>
		<td><b>Title</b></td>
		<td><b>Last edited at</b></td>
		<td><b>Public Visible</b></td>
		<td><b>Cover Text</b></td>
	</tr>
	<c:forEach items="${screenplayTemplates}" var="screenplayTemplate">
	<tr>
		<td><a href="<c:url value="/screenplaytemplate?action=edit&screenplaytemplate=${screenplayTemplate.id}"/>">${screenplayTemplate.title}</a></td>
		<td><label>${screenplayTemplate.createTime}</label></td>
		<td>
			<c:if test="${screenplayTemplate.setPublic==false}">No</c:if>
			<c:if test="${screenplayTemplate.setPublic==true}">Yes</c:if>
		</td>
		<td><label>${screenplayTemplate.preContents}</label></td>
		<td><a href="<c:url value="/screenplaytemplate?action=edit&screenplaytemplate=${screenplayTemplate.id}"/>">Edit</a></td>
		<td><a href="<c:url value="/screenplaytemplate?action=delete&screenplaytemplate=${screenplayTemplate.id}"/>">Delete</a></td>
		<td><a href="<c:url value="/screenplaytemplate?action=publicize&screenplaytemplate=${screenplayTemplate.id}"/>">Set to Public</a></td>
		<td><a href="<c:url value="/screenplaytemplate?action=unpublicize&screenplaytemplate=${screenplayTemplate.id}"/>">Set to Private</a></td>
	</tr>
	</c:forEach>
	</Table>
</div>

</body>
</html>