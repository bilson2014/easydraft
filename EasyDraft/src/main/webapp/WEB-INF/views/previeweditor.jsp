<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Preview Editor</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/preview_editor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/preview_editor/ueditor.all.js"> </script>
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/preview_editor/jquery-1.3.2.min.js"></script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="<%=request.getContextPath() %>/resources/preview_editor/lang/zh-cn/zh-cn.js"></script>

    <style type="text/css">
        .clear {
            clear: both;
        }
    </style>
</head>
<!-- 
<script type="text/javascript">

function X()
{
	alert(document.getElementById('content').innerHTML);
	alert("on submit");
	document.getElementById('preview_content').value = document.getElementById('content').innerHTML;
	
}
</script> -->

<script type="text/javascript">
	function submitPreview()
	{
		generatePreviewResult();
		
		document.getElementById("previewResult").value = document.getElementById("review").innerHTML;
		
		alert(document.getElementById("previewResult").value);
	}
	function showPreviewResult()
	{
		generatePreviewResult();
		
		$("#review").css({"display":"block"});
		/*
		UE.delEditor('editor');
		var resultContents = document.getElementById("review").innerHTML;
		alert(resultContents);
		var option = {
		        initialContent: resultContents,//初始化编辑器的内容
		        //textarea:'editorValue',//设置提交时编辑器内容的名字
		        initialFrameWidth:"80%",  //初始化编辑器宽度,默认1000，可以设置成百分比
		        initialFrameHeight:320,  //初始化编辑器高度,默认320
		        autoClearinitialContent:false  //是否自动清除编辑器初始内容
		 };
		alert(document.getElementById("review").innerHTML);
		 //实例化编辑器
		 var ue = UE.getEditor('editor',option);*/
	}
	function editPreview()
	{
		$("#review").css({"display":"none"});
	}
</script>


<body>
	<strong> please select the area you want to hide in preview look (turn a lighter color). Then click "confirm" button. </strong> 
	<form method="POST" name="preview" onsubmit="return submitPreview();">
		Preview Name : <input type="text" id="preview_name" name="preview_name" value="${previewName}"/> &nbsp;
		<!--  <input id="preview_content" name="preview_content" type="hidden" /> <br/><br/>-->
		<input type="submit" id="submitButton" name="save" value="save"/> 
		<input type="submit" id="submitButton" name="close" value="close"/>
		<script id="editor" type="text/plain" name="previewEdit" style="width:1024px;height:500px;">${previewEditData}</script>
		<input type="textarea" id="previewResult" name="previewResult" style="display:none"></input>
		<div id="review"  style="width:950px;position:relative;height:380px; position:absolute; top:0px;left:0px; z-index:10000; background:#f9f9f9; overflow:scroll;display:none; padding:20px;
	border:2px solid red"></div>
	</form>
	
	<div>
		<button onclick="clearHide()">Hide your content</button>
        <button onclick="hideText()">Cancel your "hide"</button>
        <button onclick="showPreviewResult()">Show Preview Result</button>
        <button onclick="editPreview()">Edit Preview</button>
	</div>
	
	<div id="btns">
	    <div>
	        <button onclick="getAllHtml()">获得整个html的内容</button>
	        <button onclick="getContent()">获得内容</button>
	        <button onclick="setContent()">写入内容</button>
	        <button onclick="setContent(true)">追加内容</button>
	        <button onclick="getContentTxt()">获得纯文本</button>
	        <button onclick="getPlainTxt()">获得带格式的纯文本</button>
	        <button onclick="hasContent()">判断是否有内容</button>
	        <button onclick="setFocus()">使编辑器获得焦点</button>
	        <button onmousedown="isFocus(event)" style="color:red">new 编辑器是否获得焦点</button>
	        <button onmousedown="setblur(event)" style="color:red">new 编辑器失去焦点</button>
	
	    </div>
	    <div>
	        <button onclick="getText()">获得当前选中的文本</button>
	        <button onclick="insertHtml()">插入给定的内容</button>
	        <button id="enable" onclick="setEnabled()">可以编辑</button>
	        <button onclick="setDisabled()">不可编辑</button>
	        <button onclick=" UE.getEditor('editor').setHide()">隐藏编辑器</button>
	        <button onclick=" UE.getEditor('editor').setShow()">显示编辑器</button>
	        <button onclick=" UE.getEditor('editor').setHeight(300)">设置编辑器的高度为300</button>
	    </div>
	    <div>
	        <button onclick="getLocalData()" style="color:red">new 获取草稿箱内容</button>
	        <button onclick="clearLocalData()" style="color:red">new 清空草稿箱</button>
	    </div>

	</div>
	<div>
	    <button onclick="createEditor()"/>
	    创建编辑器</button>
	    <button onclick="deleteEditor()"/>
	    删除编辑器</button>
	</div>
	
	
</body>

<script type="text/javascript">

	//PingPing
	function cancelHide(e)
    {
        UE.pattern=1;
         
    }
     function hideText(e)
    {
        UE.pattern=0;
    }
    function generatePreviewResult(e)
    {
        //$("#review").css({"display":"block"});
        //$("#close").css({"display":"block"});
        $("#review").html(UE.getEditor('editor').getContent());
        var previous,next,p,q,parentNode;
        var parentNodeAll=document.getElementById("review");
        var domList=document.getElementById("review").getElementsByTagName("*")
        {
            for(var i=0;i<domList.length;i++)
            {
                if(domList[i].tagName=="HIDE")
                {
                    var temp=domList[i];
                    while(true)
                    {
                        /*如果它的前兄弟是hide，合并*/
                        while(temp.tagName=="HIDE" && temp.nextSibling!=null && temp.nextSibling.tagName=="HIDE")
                        {
                            next=temp.nextSibling;
                            $(next).remove();
                             
                        }
                        /*如果它的后兄弟是hide，合并*/
                         while(temp.tagName=="HIDE" && temp.previousSibling && temp.previousSibling.tagName=="HIDE")
                        {
                            previous=temp.previousSibling;
                            $(previous).remove();
                            i=i-1;
                        }
                        /*如果它是第一个子节点，拿到父亲节点的前面*/
                        if(temp.nextSibling==null)
                        {   
                            while(temp.nextSibling==null && temp.parentNode!=parentNodeAll)
                            {
                                parentNode=temp.parentNode;
                                $(temp.parentNode).after(temp);
                                temp=parentNode.nextSibling;
                                //alert(parentNode.innerHTML.replace(/& lt;.+?>/gim,''));
                                if(parentNode.innerHTML.replace(/& lt;.+?>/gim,'')=="")
                                {
                                    $(parentNode).remove();
                                    i=i-1;
                                }
                                
                            }
                        }

                        /*如果它是最后一个子节点，拿到父亲节点的后面*/
                        else if(temp.previousSibling==null)
                        {
                            while(temp.previousSibling==null && temp.parentNode!=parentNodeAll)
                            {
                                parentNode=temp.parentNode;
                                $(temp.parentNode).before(temp);                            
                                temp=parentNode.previousSibling;
                                //alert(parentNode.innerHTML.replace(/& lt;.+?>/gim,''));
                                if(parentNode.innerHTML.replace(/& lt;.+?>/gim,'')=="")
                                {
                                    $(parentNode).remove();
                                    i=i-1;
                                }
                                
                            }
                        }
                        /* alert($("#review").html());*/                        
                        /*如果它的后兄弟是hide，合并*/
                        while(temp.tagName=="HIDE" && temp.nextSibling!=null && temp.nextSibling.tagName=="HIDE")
                        {
                            next=temp.nextSibling;
                            $(next).remove();
                        }
                        /*如果它的前兄弟是hide，合并*/
                         while(temp.tagName=="HIDE" && temp.previousSibling && temp.previousSibling.tagName=="HIDE")
                        {
                            
                            previous=temp.previousSibling;
                            $(previous).remove();
                            i=i-1;
                        }
                        /*如果都不是，就跳出，这个节点处理完毕*/
                       
                            break;
                        
                    }
                 }
            }
        }
		
        var path=location.pathname;
        var idx=path.indexOf("/",1);
        var ctx=path.substring(0,idx);

        $("<img src='"+ctx+"/resources/preview_editor/images/buy-now.gif'/>").replaceAll("hide");
    }
	
	//


	var option = {
        initialContent: '${contents}',//初始化编辑器的内容
        //textarea:'editorValue',//设置提交时编辑器内容的名字
        initialFrameWidth:"80%",  //初始化编辑器宽度,默认1000，可以设置成百分比
        initialFrameHeight:320,  //初始化编辑器高度,默认320
        autoClearinitialContent:false  //是否自动清除编辑器初始内容
        } 
    //实例化编辑器
    var ue = UE.getEditor('editor',option);
    function isFocus(e){
        alert(ue.isFocus());
        UE.dom.domUtils.preventDefault(e)
    }
    function setblur(e){
        ue.blur();
        UE.dom.domUtils.preventDefault(e)
    }
    function insertHtml() {
        var value = prompt('插入html代码', '');
        ue.execCommand('insertHtml', value)
    }
    function createEditor() {
        enableBtn();
        UE.getEditor('editor');
    }
    function getAllHtml() {
        alert(UE.getEditor('editor').getAllHtml())
    }
    function getContent() {
        var arr = [];
        arr.push("使用editor.getContent()方法可以获得编辑器的内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getContent());
        alert(arr.join("\n"));
    }
    function getPlainTxt() {
        var arr = [];
        arr.push("使用editor.getPlainTxt()方法可以获得编辑器的带格式的纯文本内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getPlainTxt());
        alert(arr.join('\n'))
    }
    function setContent(isAppendTo) {
        var arr = [];
        arr.push("使用editor.setContent('欢迎使用ueditor')方法可以设置编辑器的内容");
        UE.getEditor('editor').setContent('欢迎使用ueditor', isAppendTo);
        alert(arr.join("\n"));
    }
    function setDisabled() {
        UE.getEditor('editor').setDisabled('fullscreen');
        disableBtn("enable");
    }

    function setEnabled() {
        UE.getEditor('editor').setEnabled();
        enableBtn();
    }

    function getText() {
        //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
        var range = UE.getEditor('editor').selection.getRange();
        range.select();
        var txt = UE.getEditor('editor').selection.getText();
        alert(txt)
    }

    function getContentTxt() {
        var arr = [];
        arr.push("使用editor.getContentTxt()方法可以获得编辑器的纯文本内容");
        arr.push("编辑器的纯文本内容为：");
        arr.push(UE.getEditor('editor').getContentTxt());
        alert(arr.join("\n"));
    }
    function hasContent() {
        var arr = [];
        arr.push("使用editor.hasContents()方法判断编辑器里是否有内容");
        arr.push("判断结果为：");
        arr.push(UE.getEditor('editor').hasContents());
        alert(arr.join("\n"));
    }
    function setFocus() {
        UE.getEditor('editor').focus();
    }
    function deleteEditor() {
        disableBtn();
        UE.getEditor('editor').destroy();
    }
    function disableBtn(str) {
        var div = document.getElementById('btns');
        var btns = domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            if (btn.id == str) {
                domUtils.removeAttributes(btn, ["disabled"]);
            } else {
                btn.setAttribute("disabled", "true");
            }
        }
    }
    function enableBtn() {
        var div = document.getElementById('btns');
        var btns = domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            domUtils.removeAttributes(btn, ["disabled"]);
        }
    }

    function getLocalData () {
        alert(ue.execCommand( "getlocaldata" ));
    }

    function clearLocalData () {
        ue.execCommand( "clearlocaldata" );
        alert("已清空草稿箱")
    }

</script>
</html>