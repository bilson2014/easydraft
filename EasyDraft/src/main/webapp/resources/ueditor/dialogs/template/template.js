/**
 * Created with JetBrains PhpStorm.
 * User: xuheng
 * Date: 12-8-8
 * Time: 下午2:09
 * To change this template use File | Settings | File Templates.
 */
(function () {
    var me = editor,
            preview = $G( "preview" ),
            preitem = $G( "preitem" ),
            tmps = editor.options.templates,
            currentTmp;
    var initPre = function () {
        var str = "";
        for ( var i = 0, tmp; tmp = tmps[i++]; ) {
            str += '<div class="preitem" onclick="pre(' + i + ')"><img src="images/pre1.png" ' + (tmp.title ? "alt=" + tmp.title + " title=" + tmp.title + "" : "") + '></div>';
        }
        preitem.innerHTML = str;
    };
    var pre = function ( n ) {
        var tmp = tmps[n - 1];
        currentTmp = tmp;
        clearItem();
        domUtils.setStyles( preitem.childNodes[n - 1], {
            "background-color":"lemonChiffon",
            "border":"#ccc 1px solid"
        } );
        preview.innerHTML = tmp.preHtml ? tmp.preHtml : "";
    };
    var clearItem = function () {
        var items = preitem.children;
        for ( var i = 0, item; item = items[i++]; ) {
            domUtils.setStyles( item, {
                "background-color":"",
                "border":"white 1px solid"
            } );
        }
    };
    dialog.onok = function () {
        if ( !$G( "issave" ).checked ){
            me.execCommand( "cleardoc" );
        }
        var obj = {
            html:currentTmp && currentTmp.html
        };
        me.execCommand( "template", obj );
        
        //PP
        var domRoot=document.createElement("HIDE"); 
        domRoot.innerHTML=obj.html;
        //alert(domRoot.innerHTML);
        var custempstr=new Array(),ii=0;
        var noChildNodes=new Array();
        var nodeList=domRoot.getElementsByTagName("*");
        //var nodeList2=domRoot.childNodes;
        for(var i=0;i<nodeList.length;i++)
        {   
            /*如果节点没有孩子节点,也就是最里层的节点*/        
            if(nodeList[i].nodeType==1 && nodeList[i].childNodes!=null)
            {
                for(var j=0; j<nodeList[i].childNodes.length;j++ )
                {
                    if(nodeList[i].childNodes[j].nodeType==1)
                        break;
                }
                if(j==nodeList[i].childNodes.length && nodeList[i].tagName!="BR")
                noChildNodes.push(nodeList[i]);
            }
        }
        
        for(var t=0;t<noChildNodes.length;t++)
        {
              var styleTextStr=getStyleText(noChildNodes[t]);

              if(!isAleadyHas(custempstr,styleTextStr))
              {
                   var tempStr=noChildNodes[t].innerHTML;
                   var nameTempStr=tempStr.substring(tempStr.indexOf("【")+1,tempStr.indexOf("】"));
                   //var nameStr=getCustomName(nameTempStr);
                   var firstBlockParent=getFirstBlockParent(noChildNodes[t]);
                   //alert(firstBlockParent.tagName);
                   if(nameTempStr!=null && nameTempStr!="")
                   {
                      custempstr[ii++]={tag:firstBlockParent.tagName,name:nameTempStr, style:styleTextStr};
                   }
               
              }
        }
       
        //

        //alert(custempstr)
        UE.delEditor('editor');
        
        var path = location.pathname;
        var idx = path.indexOf("/",1);
        var ctx = path.substring(0,idx);
        
        $.ajax({
			type : 'GET',
			contentType : 'application/json',
			dataType : 'json',
			url : ctx + "/loadScreenplayTemplates",
			success:function(data){
				var options = {};
				options.templates = data.templates;
				options.initialContent = '${contents}';
				options.customTempStr = custempstr;
				UE.getEditor('editor',options);
			},
			error:function(data){
				alert('Can not fetch Templates');
			}
		});

    };
    initPre();
    window.pre = pre;
    pre(2);

})();


//PP
/*获取第一个块级的父亲节点*/
function getFirstBlockParent(element)
{
    var parentBlockNodeTemp=element;
    while(!UE.dom.domUtils.isBlockElm(parentBlockNodeTemp))
    {
        
        parentBlockNodeTemp=parentBlockNodeTemp.parentNode;
    }
    return parentBlockNodeTemp;
}
/*获取元素的样式字符串*/
function getStyleText(element)
{
    var styleTextStr=element.style.cssText;
    if(element.nodeName=="STRONG")
        {
            styleTextStr+="font-weight:bold;";
        }
    while(element.parentNode.nodeName!="HIDE")
    {
        styleTextStr=element.parentNode.style.cssText+styleTextStr;
        if(element.parentNode.nodeName=="STRONG")
        {
            styleTextStr+="font-weight:bold;";
        }
        element=element.parentNode;
    }
    return styleTextStr;
}
/*判断是否已经存在此样式*/
function isAleadyHas(custempstr,styleTextStr)
{
    for(var i=0;i<custempstr.length;i++)
    {

        if(custempstr[i]["style"]==styleTextStr)
            return true;
    }
    return false;

}

function getCustomName(nameStr)
{
     for(var prop in UE.I18N['zh-cn']["customstyle"])
     {
        if(UE.I18N['zh-cn']["customstyle"][prop]==nameStr)
        {
            return prop;
        }
     }
         
}
//