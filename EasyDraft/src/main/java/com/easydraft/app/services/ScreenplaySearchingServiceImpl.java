package com.easydraft.app.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.core.CoreContainer;

/**
 * @author sunbin
 *
 */
public class ScreenplaySearchingServiceImpl{
	
	String solrDir;
	String solrURL;
	boolean embeddedSolr;
	SolrServer server;
	
	public ScreenplaySearchingServiceImpl(String solrDir, String solrURL, boolean embeddedSolr) {
		super();
		this.solrDir=solrDir;
		this.solrURL=solrURL;
		this.embeddedSolr=embeddedSolr;
		
		System.out.println("Create (or Link to) the Solr Server...");
		
		if(this.embeddedSolr==true)
		{
			//Embedded Solr Server
			System.setProperty("solr.solr.home", this.solrDir);
			//CoreContainer.Initializer initializer = new CoreContainer.Initializer();
			//CoreContainer coreContainer = initializer.initialize();
			CoreContainer coreContainer = new CoreContainer(this.solrDir);
			coreContainer.load();
			this.server = new EmbeddedSolrServer(coreContainer, "");
		}
		else
		{
			//Http Solr Server
			this.server = new HttpSolrServer(this.solrURL);
		}
	}

	public String getSolrDir() {
		return solrDir;
	}

	public void setSolrDir(String solrDir) {
		this.solrDir = solrDir;
	}

	public String getSolrURL() {
		return solrURL;
	}

	public void setSolrURL(String solrURL) {
		this.solrURL = solrURL;
	}

	public boolean isEmbeddedSolr() {
		return embeddedSolr;
	}

	public void setEmbeddedSolr(boolean embeddedSolr) {
		this.embeddedSolr = embeddedSolr;
	}

	public SolrServer getServer() {
		return server;
	}

	public void setServer(SolrServer server) {
		this.server = server;
	}
	
	
	public void indexScreenplayProduction(String id, String title, String author_id, String author_showname, String author_realname, String logline, String pitch, String genres, String content_url, Date production_date){
		
		UpdateRequest updateRequest = new UpdateRequest();
		SolrInputDocument document = new SolrInputDocument(); 
		
		document.addField("id", id);
		document.addField("title", title);
		document.addField("main_author_id", author_id);
		document.addField("author_showname", author_showname);
		document.addField("author_realname", author_realname);
		document.addField("logline", logline);
		document.addField("pitch", pitch);
		document.addField("genres", genres);
		document.addField("content_url", content_url);
		document.addField("production_date", production_date);
		
		updateRequest.setAction(UpdateRequest.ACTION.COMMIT, false, false);    
	    updateRequest.add(document);
	    
	    try{
	    	UpdateResponse updateResponse = updateRequest.process(this.server);  
	        System.out.println("SOLR SERVER COMMIT STATUS = "+updateResponse.getStatus());  
	    }catch (SolrServerException e) {  
	        e.printStackTrace();  
	    } catch (IOException e){  
	        e.printStackTrace();  
	    }
	}
	
	
	public void deleteScreenplayProductionbyId(String id)
	{
		 try{
			 System.out.println("delete id = "+id);
			 UpdateResponse updateResponse = this.server.deleteByQuery("id:"+id);
			 this.server.commit();
			 System.out.println("SOLR SERVER DELETE STATUS = "+updateResponse.getStatus());  
		 }catch (SolrServerException e) {  
		        e.printStackTrace();  
		 } catch (IOException e){  
		        e.printStackTrace();  
		 }
	}
	
	public List<Map<String,String>> searchScreenplay(String searchString)
	{
		List<Map<String,String>> resultList = new ArrayList<Map<String,String>>();
		
		try{
			SolrQuery query = new SolrQuery();
			query.setQuery("text:"+searchString);
			QueryResponse rsp = this.server.query(query);
			
			Iterator<SolrDocument> iter = rsp.getResults().iterator();
			
			while(iter.hasNext())
			{
				SolrDocument resultDoc = iter.next();
				
				String draft_id = (String) resultDoc.get("id");
				
				String author_id = (String) resultDoc.get("main_author_id");
				
				Map<String,String> entry = new HashMap<String,String>();
				
				System.out.println("[Screenplay Searcher] Found screenplay [id = "+draft_id+", author_id = "+author_id+"]");
				
				entry.put("draft_id", draft_id);
				
				entry.put("author_id", author_id);
				
				resultList.add(entry);
				
			}
			
		}catch(SolrServerException e)
		{
			e.printStackTrace();
		}
		
		return resultList;
		
	}

}
