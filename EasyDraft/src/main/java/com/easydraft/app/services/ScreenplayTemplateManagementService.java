package com.easydraft.app.services;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.easydraft.app.beans.EntertainmentProfessional;
import com.easydraft.app.beans.ScreenplayTemplate;

/**
 * @author sunbin
 *
 */
public class ScreenplayTemplateManagementService {

	
	MongoOperations mongoOps;

	
	public ScreenplayTemplateManagementService(MongoOperations mongoOps) {
		super();
		this.mongoOps = mongoOps;
	}

	public ScreenplayTemplateManagementService(MongoTemplate mongoTemplate)
	{
		this.mongoOps = mongoTemplate;
	}
	
	
	public ScreenplayTemplate findScreenplayTemplateById(String id)
	{
		ScreenplayTemplate st = mongoOps.findOne(query(where("id").is(id)), ScreenplayTemplate.class);
		
		return st;
	}
	
	public List<ScreenplayTemplate> listAllScreenplayTemplates()
	{
		List<ScreenplayTemplate> sts = mongoOps.findAll(ScreenplayTemplate.class);
		
		return sts;
	}
	
	public List<ScreenplayTemplate> listAllScreenplayTemplatesForEntertainmentProfessional(String epid)
	{
		List<ScreenplayTemplate> sts = mongoOps.find(query(where("entertainmentProfessionalId").is(epid)), ScreenplayTemplate.class);
		
		return sts;
	}
	
	public List<ScreenplayTemplate> listAllPublicScreenplayTemplates()
	{
		List<ScreenplayTemplate> sts = mongoOps.find(query(where("setPublic").is(true)),  ScreenplayTemplate.class);
		
		return sts;
	}
	
	public String createScreenplayTemplate(String epid, String title, String imageURL, String preContents, String contents)
	{
		ScreenplayTemplate st  = new ScreenplayTemplate();
		
		st.setEntertainmentProfessionalId(epid);
		
		st.setTitle(title);
		
		st.setImageURL(imageURL);
		
		st.setSetPublic(false);
		
		st.setPreContents(preContents);
		
		st.setContents(contents);
		
		st.setCreateTime(new Date(System.currentTimeMillis()));
		
		mongoOps.insert(st);
		
		if(st.getId()!=null)
		{
			UserManagementService ums = new UserManagementService(this.mongoOps);
		
			EntertainmentProfessional epToUpdate = ums.findEntertainmentProfessionalById(epid);
			
			epToUpdate.addScreenplayTemplates(st.getId());
			
			mongoOps.save(epToUpdate);
		
			System.out.println("Screenplay Template added, id = "+st.getId()+", epid = "+epid+", title = "+title);
			
			return st.getId();
		}
		else
			return null;
	}
	
	
	public void deleteScreenplayTemplate(String screenplayTemplateId)
	{
		ScreenplayTemplate st = this.findScreenplayTemplateById(screenplayTemplateId);
		
		String epid = st.getEntertainmentProfessionalId();
		
		mongoOps.remove(query(where("id").is(screenplayTemplateId)), ScreenplayTemplate.class);
		
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		EntertainmentProfessional epToUpdate = ums.findEntertainmentProfessionalById(epid);
		
		epToUpdate.deleteScreenplayTemplates(screenplayTemplateId);
		
		mongoOps.save(epToUpdate);
	}
	
	public void updateExistingScreenplayTemplate(String screenplayTemplateId, String title, String imageURL, String preContents, String contents)
	{
		ScreenplayTemplate st = this.findScreenplayTemplateById(screenplayTemplateId);
		
		st.setTitle(title);
		
		st.setImageURL(imageURL);
		
		st.setPreContents(preContents);
		
		st.setContents(contents);
		
		mongoOps.save(st);
	}
	
	public void publicizeScreenplayTemplate(String screenplayTemplateId)
	{
		ScreenplayTemplate st = this.findScreenplayTemplateById(screenplayTemplateId);
		
		st.setSetPublic(true);
		
		mongoOps.save(st);
	}
	
	public void unpublicizeScreenplayTemplate(String screenplayTemplateId)
	{
		ScreenplayTemplate st = this.findScreenplayTemplateById(screenplayTemplateId);
		
		st.setSetPublic(false);
		
		mongoOps.save(st);
	}
}
