package com.easydraft.app.services;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

import java.util.List;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.easydraft.app.beans.EntertainmentProfessional;
import com.easydraft.app.beans.User;

/**
 * @author sunbin
 *
 */
public class UserManagementService {

	MongoOperations mongoOps;
	
	
	public UserManagementService(MongoOperations mongoOps) {
		super();
		this.mongoOps = mongoOps;
	}

	public UserManagementService(MongoTemplate mongoTemplate)
	{
		mongoOps = mongoTemplate;
	}

	
	public User userLogin(String username, String password){
		User p = mongoOps.findOne(query(where("showName").is(username).and("password").is(password)),User.class);
		return p;
	}
	
	public Boolean register(User user){
		
		if(isUserExist(user.getShowName()))
			return false;
		else
		{
			mongoOps.insert(user);
			 
			if(isUserExist(user.getShowName()))
				return true;
			else
				return false;
		}
	}
	
	public Boolean isUserExist(String showName){
		
		List<User> users = mongoOps.find(query(where("showName").is(showName)),User.class);
		
		if(users.isEmpty())
			return false;
		else
			return true;
	}
	
	public User findUserById(String id)
	{
		User u = mongoOps.findOne(query(where("id").is(id)), User.class);
		
		return u;
	}
	
	public List<User> listAllUsers()
	{
		List<User> users = mongoOps.findAll(User.class);
		
		return users;
	}
	
	
	
	public EntertainmentProfessional entertainmentProfessionalLogin(String username, String password){
		EntertainmentProfessional ep = mongoOps.findOne(query(where("showName").is(username).and("password").is(password)),EntertainmentProfessional.class);
		return ep;
	}
	
		
	public Boolean register(EntertainmentProfessional entertainmentProfessional){
		
		if(isEntertainmentProfessionalExist(entertainmentProfessional.getShowName()))
			return false;
		else
		{
			mongoOps.insert(entertainmentProfessional);
			
			if(isEntertainmentProfessionalExist(entertainmentProfessional.getShowName()))
				return true;
			else
				return false;
		}
	}
	
	public Boolean isEntertainmentProfessionalExist(String showName){
		
		List<EntertainmentProfessional> eps = mongoOps.find(query(where("showName").is(showName)),EntertainmentProfessional.class);
	
		if(eps.isEmpty())
			return false;
		else
			return true;
	}
	
	public EntertainmentProfessional findEntertainmentProfessionalById(String id)
	{
		EntertainmentProfessional ep = mongoOps.findOne(query(where("id").is(id)), EntertainmentProfessional.class);
		
		return ep;
	}
	
}



