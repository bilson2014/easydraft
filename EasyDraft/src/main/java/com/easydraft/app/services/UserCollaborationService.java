package com.easydraft.app.services;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.User;
import com.mongodb.gridfs.GridFSDBFile;

/**
 * @author sunbin
 *
 */
public class UserCollaborationService {

	MongoOperations mongoOps;
	
	MongoDbFactory mongoDbFactory;

	public UserCollaborationService(MongoOperations mongoOps, MongoDbFactory mongoDbFactory) {
		super();
		this.mongoOps = mongoOps;
		this.mongoDbFactory= mongoDbFactory;
	}
	
	public UserCollaborationService(MongoTemplate mongoTemplate, MongoDbFactory mongoDbFactory)
	{
		this.mongoOps = mongoTemplate;
		this.mongoDbFactory= mongoDbFactory;
	}
	
	public List<User> findAllCollaborators(String draft_id, String user_id)
	{
		UserManagementService ums = new UserManagementService(mongoOps);
		
		User user = ums.findUserById(user_id);
		
		Draft draft = user.searchDraftById(draft_id);
		
		List<User> collaborators = new ArrayList<User>();
		
		if(!draft.getMainAuthorId().equals(user_id))
		{
			//add Main Author
			collaborators.add(ums.findUserById(draft.getMainAuthorId()));
		}
		
		if(draft.getCoAuthorIds()!=null)
		{
			for(String id : draft.getCoAuthorIds())
			{
				if(!id.equals(user_id))
				{
					//add writers
					collaborators.add(ums.findUserById(id));
				}
			}
		}
		
		return collaborators;
	}
	
	public List<Draft> findAllCollaboratedDrafts(String draft_id, List<User> collaboratedWriters)
	{
		List<Draft> allCollaboratedDrafts = new ArrayList<Draft>();
		
		for(User writer : collaboratedWriters)
		{
			//identical id for all duplications
			Draft d = writer.searchDraftById(draft_id);
			
			allCollaboratedDrafts.add(d);
		}
		
		return allCollaboratedDrafts;
	}
	
	public List<User> findAllOtherWriters(String draft_id, String user_id)
	{
		List<User> otherWriters = new ArrayList<User>();
		
		UserManagementService ums = new UserManagementService(mongoOps);
		
		List<User> allWriters = ums.listAllUsers();
		
		User currentWriter = ums.findUserById(user_id);
		
		Draft currentDraft = currentWriter.searchDraftById(draft_id);
		
		List<String> existingWriterIds = new ArrayList<String>();
		
		existingWriterIds.add(currentDraft.getMainAuthorId());
		
		if(currentDraft.getCoAuthorIds()!=null)
		{
			for(String writerId : currentDraft.getCoAuthorIds())
			{
				existingWriterIds.add(writerId);
			}
		}
		
		for(User writer : allWriters)
		{
			if(!existingWriterIds.contains(writer.getId()))
			{
				otherWriters.add(writer);
			}
		}
		
		return otherWriters;
	}
	
	
	public void addCollaboratorToDraft(String draft_id, String collaborator_id, String originalWriter_id)
	{
		UserManagementService ums = new UserManagementService(mongoOps);
		
		//on main author side, just add the collaborator id
		User mainAuthor = ums.findUserById(originalWriter_id);
		
		Draft draft = mainAuthor.searchDraftById(draft_id);
		
		if(draft.getCoAuthorIds()==null) draft.setCoAuthorIds(new ArrayList<String>());
		
		draft.getCoAuthorIds().add(collaborator_id);
		
		
		//on collaborator side, we need create the draft and create the content file
		User collaborator = ums.findUserById(collaborator_id);
		
		Draft dup_draft = new Draft();
		
		//the link between draft and duplicated draft is using "id"
		dup_draft.setId(draft.getId());
		
		dup_draft.setMainAuthorName(mainAuthor.getFirstName());
		
		dup_draft.setMainAuthorId(mainAuthor.getId());
		
		dup_draft.setCoAuthorIds(draft.getCoAuthorIds());
		
		Date now = new Date(System.currentTimeMillis());
		
		dup_draft.setCreateTime(now);
		
		dup_draft.setVersion(now.toString());
		
		dup_draft.setLastEditTime(now);
		
		dup_draft.setTitle(draft.getTitle());
		
		//duplicate the file copy.
		FileManagementService fms = new FileManagementService(mongoDbFactory);
		
		GridFSDBFile file = fms.findFileByName(draft.getExternalURL());
		
		InputStream copyStream = file.getInputStream();
		
		dup_draft.generateExternalURL();
		
		fms.saveFile(copyStream, dup_draft.getExternalURL());
		
		collaborator.addDraft(dup_draft);
		
		//save ..
		
		mongoOps.save(mainAuthor);
		
		mongoOps.save(collaborator);
	}
	
	
	public void removeCollaborator(String draft_id, String collaborator_id)
	{
		UserManagementService ums = new UserManagementService(mongoOps);
		
		User collaborator = ums.findUserById(collaborator_id);
		
		Draft draft = collaborator.searchDraftById(draft_id);
		
		User mainAuthor = ums.findUserById(draft.getMainAuthorId());
		
		if(mainAuthor!=null) //main author still exist
		{
			Draft originalDraft = mainAuthor.searchDraftById(draft_id);
			
			originalDraft.getCoAuthorIds().remove(collaborator_id);
			
			mongoOps.save(mainAuthor);
		}
		
		//remove the copy
		FileManagementService fms = new FileManagementService(mongoDbFactory);
		
		fms.deleteFileByName(draft.getExternalURL());
		
		collaborator.deleteDraft(draft);
		
		mongoOps.save(collaborator);
	}
}
