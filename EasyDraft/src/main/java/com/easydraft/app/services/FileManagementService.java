package com.easydraft.app.services;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.data.mongodb.MongoDbFactory;

import com.mongodb.DB;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;

/**
 * @author sunbin
 *
 */
public class FileManagementService {


	//File management use Mongo Java Driver (not Spring data)
	DB db;
	
	public FileManagementService(MongoDbFactory mongoDbFactory)
	{
		db = mongoDbFactory.getDb();
	}

	public FileManagementService(DB db) {
		super();
		this.db = db;
	}
	
    public boolean saveFile(InputStream in, String fileName) { 
          GridFSInputFile gfsInput;  
          try {
        	  if(fileName==null) throw new Exception("Null FileName Exception");
        	  
        	  gfsInput = new GridFS(db, "draftstore").createFile(in);  
	          gfsInput.setFilename(fileName);
	          gfsInput.save();  
          } catch (Exception e) {  
        	  e.printStackTrace();  
        	  return false;  
          }  
          return true;  
    } 
    
    public GridFSDBFile findFileByName(String fileName){  
        GridFSDBFile gfsFile ;  
        try {        
        	gfsFile = new GridFS(db, "draftstore").findOne(fileName);  
        } catch (Exception e) {  
        	e.printStackTrace(); 
        	return null;  
        }  
        return gfsFile; 
    }
    
    public boolean writeToOutputStream(OutputStream out, GridFSDBFile gfsFile) {
    	if(gfsFile == null) return true;
    	
        try {       
        	gfsFile.writeTo(out);  
        } catch (IOException e) {  
        	e.printStackTrace();  
          return false;  
        }  
        return true;  
    }
    
    public boolean deleteFileByName(String fileName){
    	
    	if(fileName==null) return true;
    	
    	try{
    		new GridFS(db, "draftstore").remove(fileName);
    	} catch (Exception e){
    		e.printStackTrace();
    		return false;
    	}
    	return true;
    }
	
}
