package com.easydraft.app.services;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.DraftPreview;
import com.easydraft.app.beans.DraftProduction;
import com.easydraft.app.beans.User;

/**
 * @author sunbin
 *
 */
public class ProductionService {

	MongoOperations mongoOps;
	
	MongoDbFactory mongoDbFactory;

	public ProductionService(MongoOperations mongoOps, MongoDbFactory mongoDbFactory) {
		super();
		this.mongoOps = mongoOps;
		this.mongoDbFactory = mongoDbFactory;
	}
	
	public ProductionService(MongoTemplate mongoTemplate, MongoDbFactory mongoDbFactory) {
		super();
		this.mongoOps = mongoTemplate;
		this.mongoDbFactory = mongoDbFactory;
	}
	
	public DraftProduction getProductionInformation(String draft_id, String user_id)
	{
		UserManagementService ums = new UserManagementService(mongoOps);
		
		User user = ums.findUserById(user_id);
		
		Draft draft = user.searchDraftById(draft_id);
		
		if(draft.getProduction()==null) //generate new production
		{
			draft.setProduction(new DraftProduction());
			
			mongoOps.save(user);
		}
		
		return draft.getProduction();
	}
	
	public int saveProductionInfo(String draft_id, String user_id,String pitch,String logline, DraftProduction.Genres genre)
	{
		UserManagementService ums = new UserManagementService(mongoOps);
		
		User user = ums.findUserById(user_id);
		
		if(user!=null)
		{
			Draft draft = user.searchDraftById(draft_id);
		
			if(draft!=null)
			{
				if(draft.getProduction()!=null)
				{
					//save production
					draft.getProduction().setPitch(pitch);
					
					draft.getProduction().setLogline(logline);
					
					draft.getProduction().setGenre(genre);
					
					mongoOps.save(user);
					
					return 0;
				}
				else//production is not existing
					return 3;
			}
			else//draft is not existing
				return 1;
		}
		else //user is not existing
			return 2;
	}
	
	public String createDraftPreview(String draft_id,String user_id){
		
		UserManagementService ums = new UserManagementService(mongoOps);
		
		User user = ums.findUserById(user_id);
		
		if(user!=null)
		{
			Draft draft = user.searchDraftById(draft_id);
			
			if(draft!=null)
			{
				DraftProduction dp = draft.getProduction();
				
				if(dp!=null)
				{
					DraftPreview preview = new DraftPreview();
					
					preview.generateId(user_id);
					
					dp.addPreview(preview);
					
					mongoOps.save(user);
					
					return preview.getId();
				}
			}
		}
		
		return null;
	}
	
	public boolean deletePreview(String draft_id,String user_id,String preview_id)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User user = ums.findUserById(user_id);
		
		if(user==null) return false;
		
		Draft draft = user.searchDraftById(draft_id);
		
		if(draft==null) return false;
		
		DraftProduction dp = draft.getProduction();
		
		if(dp==null) return false;
		
		DraftPreview p = dp.searchPreviewById(preview_id);
		
		if(p==null) return false;
		
		if(fms.deleteFileByName(p.getPreviewURL())&&fms.deleteFileByName(p.getPreviewGenerationURL()))
		{
			dp.deletePreviewById(preview_id);
			
			if(dp.getExposedPreviewId()!=null && dp.getExposedPreviewId().equals(preview_id))
				dp.setExposedPreviewId("");
			
			mongoOps.save(user);
			
			return true;
		}
		else
			return false;
		
	}
	
	public boolean setPreview(String draft_id, String user_id,String preview_id)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User user = ums.findUserById(user_id);
		
		if(user==null) return false;
		
		Draft draft = user.searchDraftById(draft_id);
		
		if(draft==null) return false;
		
		DraftProduction dp = draft.getProduction();
		
		if(dp==null) return false;
		
		DraftPreview p = dp.searchPreviewById(preview_id);
		
		if(p==null) return false;
		
		dp.setExposedPreviewId(preview_id);
		
		mongoOps.save(user);
		
		return true;
	}
	
	public int viewPreviewResult(String draft_id, String user_id, String preview_id, OutputStream contentOutput)
	{
		UserManagementService ums = new UserManagementService(mongoOps);
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User user = ums.findUserById(user_id);
		
		if(user!=null)
		{
			Draft draft = user.searchDraftById(draft_id);
		
			if(draft!=null)
			{
				DraftProduction dp = draft.getProduction();
				
				if(dp!=null)
				{
					DraftPreview preview = dp.searchPreviewById(preview_id);
						
					if(preview!=null)
					{
						fms.writeToOutputStream(contentOutput, fms.findFileByName(preview.getPreviewURL()));
						
						return 0;
					}
					else//preview is not existing		
						return 4;
				}
				else//production is not existing
					return 3;
			}
			else//draft is not existing
				return 1;
		}
		else //user is not existing
			return 2;
	}
	
	public int getPreviewWork(String draft_id, String user_id, String preview_id, OutputStream contentOutput)
	{
		UserManagementService ums = new UserManagementService(mongoOps);
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User user = ums.findUserById(user_id);
		
		if(user!=null)
		{
			Draft draft = user.searchDraftById(draft_id);
		
			if(draft!=null)
			{
				DraftProduction dp = draft.getProduction();
				
				if(dp!=null)
				{
					DraftPreview preview = dp.searchPreviewById(preview_id);
						
					if(preview!=null)
					{
						fms.writeToOutputStream(contentOutput, fms.findFileByName(preview.getPreviewGenerationURL()));
						
						return 0;
					}
					else//preview is not existing		
						return 4;
				}
				else//production is not existing
					return 3;
			}
			else//draft is not existing
				return 1;
		}
		else //user is not existing
			return 2;
	}
	
	public int savePreview(String draft_id, String user_id, String preview_id, String preview_name, String editedPreviewContent, String generatedPreviewContent)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User user = ums.findUserById(user_id);
		
		if(user!=null)
		{
			Draft draft = user.searchDraftById(draft_id);
		
			if(draft!=null)
			{
				DraftProduction dp = draft.getProduction();
				
				if(dp!=null)
				{
					DraftPreview preview = dp.searchPreviewById(preview_id);
						
					if(preview!=null)
					{
						preview.setPreviewName(preview_name);
						
						try{
							InputStream editedPreviewContentInput = new ByteArrayInputStream(editedPreviewContent.getBytes("UTF-8"));
							
							InputStream generatedPreviewContentInput = new ByteArrayInputStream(generatedPreviewContent.getBytes("UTF-8"));
							
							String existingEditedPreviewFileName = preview.getPreviewGenerationURL();
							
							String existingGeneratedPreviewFileName = preview.getPreviewURL();
							
							
							//create new file
						
							preview.generatePreviewGenerationURL();
							
							preview.generatePreviewURL();
							
							fms.saveFile(editedPreviewContentInput, preview.getPreviewGenerationURL());
							
							fms.saveFile(generatedPreviewContentInput, preview.getPreviewURL());
							
							//delete the existing file
							fms.deleteFileByName(existingEditedPreviewFileName);
							
							fms.deleteFileByName(existingGeneratedPreviewFileName);
							
						}catch(Exception e)
						{
							e.printStackTrace();
						}
						
						mongoOps.save(user);
						
						return 0;
					}
					else//preview is not existing		
						return 4;
				}
				else//production is not existing
					return 3;
			}
			else//draft is not existing
				return 1;
		}
		else //user is not existing
			return 2;
		
	}
}
