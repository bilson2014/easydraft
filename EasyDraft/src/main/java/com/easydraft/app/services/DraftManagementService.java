package com.easydraft.app.services;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.User;

/**
 * @author sunbin
 *
 */
public class DraftManagementService {
	
	MongoOperations mongoOps;
	
	MongoDbFactory mongoDbFactory;
	
	public DraftManagementService(MongoOperations mongoOps, MongoDbFactory mongoDbFactory) {
		super();
		this.mongoOps = mongoOps;
		this.mongoDbFactory = mongoDbFactory;
	}

	public DraftManagementService(MongoTemplate mongoTemplate, MongoDbFactory mongoDbFactory)
	{
		this.mongoOps = mongoTemplate;
		this.mongoDbFactory = mongoDbFactory;
	}
	
	public String CreateNewDraft(User mainAuthor)
	{
		
		Draft draft = new Draft();
		
		draft.setMainAuthorId(mainAuthor.getId());
		
		draft.setMainAuthorName(mainAuthor.getFirstName());
		
		Date now = new Date(System.currentTimeMillis());
		
		draft.setCreateTime(now);
		
		draft.setVersion(now.toString());
		
		draft.generateId();
		
		
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		User userToUpdate = ums.findUserById(mainAuthor.getId());
		
		userToUpdate.addDraft(draft);
		
		/*
		Update update = new Update();
		
		update.set("createdDraft",mainAuthor.getCreatedDraft());
		
		//update the user to link with the new draft
		mongoOps.updateFirst(query(where("id").is(mainAuthor.getId())), update, User.class);*/
		
		mongoOps.save(userToUpdate);
		
		//find the last inserted draft.
		return draft.getId();
	}
	
	
	public int UpdateExistingDraft(String draft_id, String title, String content, User editor)
	{
		/*
		Update update = new Update();
		
		update.set("title", draft.getTitle());
		
		update.set("lastEditorId", user.getId());
		
		update.set("lastEditorName", user.getFirstName());
		
		update.set("lastEditTime", new Date(System.currentTimeMillis()));
		
		return mongoOps.updateFirst(query(where("id").is(draft.getId())), update, Draft.class);*/
		
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User userToUpdate = ums.findUserById(editor.getId());
		
		
		if(userToUpdate!=null)
		{
			Draft d = userToUpdate.searchDraftById(draft_id);
		
			if(d!=null) //find the document to up-to-update
			{
				d.setTitle(title);
				
				Date now = new Date(System.currentTimeMillis());
				
				d.setVersion(now.toString());
				
				d.setLastEditTime(now);
				
				try{
					InputStream contentInput = new ByteArrayInputStream(content.getBytes("UTF-8"));
					
					String existingFileName = d.getExternalURL();
					
					//create new file
					d.generateExternalURL();
				
					fms.saveFile(contentInput, d.getExternalURL());
					
					//delete the existing file
					fms.deleteFileByName(existingFileName);
					
				}catch(Exception e)
				{
					e.printStackTrace();
				}
				
				mongoOps.save(userToUpdate);
				
				return 0;
			}else
			{
				//draft is not existing.
				return 1;
			}
			
		}else
			//user is not existing.
			return 2;
		
	}
	
	public Draft getExistingDraft(String draft_id, User author, OutputStream contentOutput)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User user = ums.findUserById(author.getId());
		
		
		if(user!=null)
		{
			Draft d= user.searchDraftById(draft_id);
			
			fms.writeToOutputStream(contentOutput, fms.findFileByName(d.getExternalURL()));
			
			return d;
		}
		else
			
			return null;
		
	}
	
	
	public boolean deleteDraft(String draft_id,User author)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		FileManagementService fms = new FileManagementService(this.mongoDbFactory);
		
		User userToUpdate = ums.findUserById(author.getId());
		
		if(userToUpdate==null) return false;
		
		Draft draftToBeDeleted = userToUpdate.searchDraftById(draft_id);
		
		if(draftToBeDeleted==null) return false;
		
		if(!fms.deleteFileByName(draftToBeDeleted.getExternalURL())) return false;
		
		userToUpdate.deleteDraftById(draft_id);
		
		mongoOps.save(userToUpdate);
		
		return true;
	}
}
