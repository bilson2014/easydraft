package com.easydraft.app.services;

import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.easydraft.app.beans.DraftExchange;
import com.easydraft.app.beans.EntertainmentProfessional;

public class DraftExchangeManagementService {

	MongoOperations mongoOps;
	
	MongoDbFactory mongoDbFactory;
	
	public DraftExchangeManagementService(MongoOperations mongoOps, MongoDbFactory mongoDbFactory) {
		super();
		this.mongoOps = mongoOps;
		this.mongoDbFactory = mongoDbFactory;
	}

	public DraftExchangeManagementService(MongoTemplate mongoTemplate, MongoDbFactory mongoDbFactory)
	{
		this.mongoOps = mongoTemplate;
		this.mongoDbFactory = mongoDbFactory;
	}
	
	public String createNewDraftExchange(String epid, String authorId, String draftId)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		EntertainmentProfessional epToUpdate = ums.findEntertainmentProfessionalById(epid);
		
		if(epToUpdate!=null)
		{
			//see if the draft is already added
			DraftExchange deExisted = epToUpdate.findDraftExchange(authorId, draftId);
			if(deExisted!=null) return "existed";
			
			DraftExchange de = new DraftExchange();
			de.setEntertainmentProfessionalId(epid);
			de.setMainAuthorId(authorId);
			de.setDraftId(draftId);
			de.setAuthroizationLevel(0);
			de.setLevelOfInterest(DraftExchange.HotLevel.values()[0]);
			
			de.generateId();
			
			epToUpdate.addDraftExchange(de);
			
			mongoOps.save(epToUpdate);
			
			System.out.println("Draft Exchange added, id = "+de.getId()+", epid = "+epid+", authorid = "+authorId+", draftid = "+draftId);
				
			return de.getId();
		}
		else
			return null;
		
	}
	
	public boolean buyDraftExchange(String epid, String deid)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		EntertainmentProfessional epToUpdate = ums.findEntertainmentProfessionalById(epid);
		
		if(epToUpdate!=null)
		{
			DraftExchange de = epToUpdate.findDraftExchangeById(deid);
			
			if(de!=null)
			{
				de.setAuthroizationLevel(DraftExchange.AUTHORIZED);
				
				//some other actions. maybe need to copy.
				
				mongoOps.save(epToUpdate);
				
				System.out.println("Authorized EntertainmentProfessional id = "+epid+" to access DraftExchange id="+deid);
				
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	
	public boolean removeDraftExchange(String epid, String deid)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		EntertainmentProfessional epToUpdate = ums.findEntertainmentProfessionalById(epid);
		
		if(epToUpdate!=null)
		{
			epToUpdate.deleteDraftExchangeById(deid);
				
			mongoOps.save(epToUpdate);
				
			return true;	
		}
		else
			return false;
	}
	
	public boolean changeInterestLevel(String epid,String deid,boolean increase)
	{
		UserManagementService ums = new UserManagementService(this.mongoOps);
		
		EntertainmentProfessional epToUpdate = ums.findEntertainmentProfessionalById(epid);
		
		if(epToUpdate!=null)
		{
			DraftExchange de = epToUpdate.findDraftExchangeById(deid);
			if(increase)
			{
				int i=0;
				boolean found=false;
				
				while(i<(DraftExchange.HotLevel.values().length-1) && !found)
				{
					int result = de.getLevelOfInterest().compareTo(DraftExchange.HotLevel.values()[i]);
					
					if(result<0)
					{
						return false;						
					}else if(result==0)
					{
						found = true;						
					}else
					{
						i++;
					}
				}
				
				if(found) de.setLevelOfInterest(DraftExchange.HotLevel.values()[i+1]);
			}else //decrease
			{
				int i= DraftExchange.HotLevel.values().length-1;
				
				boolean found = false;
				
				while(i>0 && !found)
				{
					int result = de.getLevelOfInterest().compareTo(DraftExchange.HotLevel.values()[i]);
					
					if(result<0)
					{
						i--;
					}else if(result==0)
					{
						found = true;
					}else
					{
						return false;
					}
				}
				
				if(found) de.setLevelOfInterest(DraftExchange.HotLevel.values()[i-1]);
			}
			
			mongoOps.save(epToUpdate);
			
			return true;
			
		}else
			return false;
		
		
	}
}
