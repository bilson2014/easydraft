
package com.easydraft.app.beans;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.security.*;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author sunbin
 *
 */
@Document
public class Draft implements Serializable {
	@Id
	private String id;
	private String title;
	private String version;
	
	private Date createTime;
	private Date lastEditTime;
	
	private String mainAuthorId;
	private String mainAuthorName;
	
	private List<String> coAuthorIds;
	private String externalURL;
	
	private DraftProduction production;
	
	public Draft() {
		super();
	}
	
	
	public Draft(String id, String title, String version, Date createTime,
			Date lastEditTime, String mainAuthorId, String mainAuthorName,
			List<String> coAuthorIds, String externalURL,
			DraftProduction production) {
		super();
		this.id = id;
		this.title = title;
		this.version = version;
		this.createTime = createTime;
		this.lastEditTime = lastEditTime;
		this.mainAuthorId = mainAuthorId;
		this.mainAuthorName = mainAuthorName;
		this.coAuthorIds = coAuthorIds;
		this.externalURL = externalURL;
		this.production = production;
	}


	public void generateExternalURL()
	{
		this.externalURL = this.getTitle().replace(' ', '_')+this.getLastEditTime().getTime();
	}
	

	public void addCoAuthor(User coAuthor)
	{
		if(this.coAuthorIds == null) this.coAuthorIds = new ArrayList<String>();
		
		this.coAuthorIds.add(coAuthor.getId());
	}
	
	public void generateId()
	{
		String idToBeEncoded = this.mainAuthorId+this.createTime.getTime();
	
		try{
			
			byte[] byteIdToBeEncoded = idToBeEncoded.getBytes("UTF-8");

			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			
			byte[] md = mdInst.digest(byteIdToBeEncoded);
			
			//byte[] to hex-char[] -> 1 byte to 2 digits hex number
			char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
			int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
			
			this.id = new String(str);
			
		}catch(Exception e)
		{
			e.printStackTrace();
			
			this.id = new String(idToBeEncoded);
		}
	}
	
	
	public DraftProduction getProduction() {
		return production;
	}


	public void setProduction(DraftProduction production) {
		this.production = production;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public String getMainAuthorName() {
		return mainAuthorName;
	}

	public void setMainAuthorName(String mainAuthorName) {
		this.mainAuthorName = mainAuthorName;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public Date getLastEditTime() {
		return lastEditTime;
	}


	public void setLastEditTime(Date lastEditTime) {
		this.lastEditTime = lastEditTime;
	}


	public String getMainAuthorId() {
		return mainAuthorId;
	}


	public void setMainAuthorId(String mainAuthorId) {
		this.mainAuthorId = mainAuthorId;
	}


	public List<String> getCoAuthorIds() {
		return coAuthorIds;
	}


	public void setCoAuthorIds(List<String> coAuthorIds) {
		this.coAuthorIds = coAuthorIds;
	}


	public String getExternalURL() {
		return externalURL;
	}


	public void setExternalURL(String externalURL) {
		this.externalURL = externalURL;
	}
	
	
	
}
