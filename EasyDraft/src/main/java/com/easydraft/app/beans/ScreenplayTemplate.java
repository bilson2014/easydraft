package com.easydraft.app.beans;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
/**
 * @author sunbin
 *
 */
@Document
public class ScreenplayTemplate implements Serializable {

	@Id
	private String id;
	private String entertainmentProfessionalId;
	private String title;
	private String imageURL;
	private Date createTime;
	private boolean setPublic;
	private String preContents;
	private String contents;
	
	
	public ScreenplayTemplate() {
		super();
	}


	public ScreenplayTemplate(String id, String entertainmentProfessionalId,
			String title, String imageURL, Date createTime, boolean setPublic,
			String preContents, String contents) {
		super();
		this.id = id;
		this.entertainmentProfessionalId = entertainmentProfessionalId;
		this.title = title;
		this.imageURL = imageURL;
		this.createTime = createTime;
		this.setPublic = setPublic;
		this.preContents = preContents;
		this.contents = contents;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getEntertainmentProfessionalId() {
		return entertainmentProfessionalId;
	}


	public void setEntertainmentProfessionalId(String entertainmentProfessionalId) {
		this.entertainmentProfessionalId = entertainmentProfessionalId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getImageURL() {
		return imageURL;
	}


	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}


	public Date getCreateTime() {
		return createTime;
	}


	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	public boolean isSetPublic() {
		return setPublic;
	}


	public void setSetPublic(boolean setPublic) {
		this.setPublic = setPublic;
	}


	public String getPreContents() {
		return preContents;
	}


	public void setPreContents(String preContents) {
		this.preContents = preContents;
	}


	public String getContents() {
		return contents;
	}


	public void setContents(String contents) {
		this.contents = contents;
	}
	
	
}
