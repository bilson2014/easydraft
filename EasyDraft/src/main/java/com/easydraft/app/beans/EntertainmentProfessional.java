package com.easydraft.app.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author sunbin
 *
 */
@Document
public class EntertainmentProfessional implements Serializable {

	@Id
	String id;
	private String showName;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private String locationLine1;
	private String locationLine2;
	private String city;
	private Country country;
	private String postCode;
	
	//special
	private IndustryArea type;
	private String directPhone;
	private String directFax;
	private String iMDBLink;
	private String industryReferences;
	
	private List<DraftExchange> shortListedDrafts; 
	private List<String> screenplayTemplates;
	
	public enum Country{
		China, United_Kingdom, United_States
	}
	
	public enum IndustryArea{
		Agent, Manager, ProductionCompany, OtherIndustryProfessional
	}
	
	

	public EntertainmentProfessional() {
		super();
	}

	
	public EntertainmentProfessional(String id, String showName,
			String password, String firstName, String lastName, String email,
			String locationLine1, String locationLine2, String city,
			Country country, String postCode, IndustryArea type,
			String directPhone, String directFax, String iMDBLink,
			String industryReferences, List<DraftExchange> shortListedDrafts,
			List<String> screenplayTemplates) {
		super();
		this.id = id;
		this.showName = showName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.locationLine1 = locationLine1;
		this.locationLine2 = locationLine2;
		this.city = city;
		this.country = country;
		this.postCode = postCode;
		this.type = type;
		this.directPhone = directPhone;
		this.directFax = directFax;
		this.iMDBLink = iMDBLink;
		this.industryReferences = industryReferences;
		this.shortListedDrafts = shortListedDrafts;
		this.screenplayTemplates = screenplayTemplates;
	}


	
	public List<DraftExchange> getInterestList(){
		
		if(this.shortListedDrafts==null) return null;
		List<DraftExchange> interestList = new ArrayList<DraftExchange>();
		for(DraftExchange de:this.shortListedDrafts){
			if(de.getAuthroizationLevel()!=DraftExchange.AUTHORIZED)
			{
				interestList.add(de);
			}
		}
		return interestList;
	}
	
	public List<DraftExchange> getPurchasedList(){
		
		if(this.shortListedDrafts==null) return null;
		List<DraftExchange> purchasedList = new ArrayList<DraftExchange>();
		for(DraftExchange de:this.shortListedDrafts){
			if(de.getAuthroizationLevel()==DraftExchange.AUTHORIZED)
			{
				purchasedList.add(de);
			}
		}
		return purchasedList;
	}
	
	public void addDraftExchange(DraftExchange de)
	{
		if(this.shortListedDrafts==null) this.shortListedDrafts = new ArrayList<DraftExchange>();
		this.shortListedDrafts.add(de);
	}
	
	public void deleteDraftExchangeById(String id)
	{
		if(this.shortListedDrafts==null) return;
		DraftExchange toBeDeleted = null;
		for(DraftExchange de:this.shortListedDrafts)
		{
			if(de.getId().equals(id))
			{
				toBeDeleted = de;
				
				break;
			}
		}
		
		this.deleteDraftExchange(toBeDeleted);
		
	}
	
	public void deleteDraftExchange(DraftExchange de)
	{
		if(this.shortListedDrafts==null) return;
		this.shortListedDrafts.remove(de);
	}
	
	public DraftExchange findDraftExchange(String author,String draft)
	{
		if(this.shortListedDrafts==null) return null;
		
		for(DraftExchange de:this.shortListedDrafts)
		{
			if(de.getMainAuthorId().equals(author)&&de.getDraftId().equals(draft))
			{
				return de;
			}
		}
		
		return null;
	}
	
	public DraftExchange findDraftExchangeById(String draftExchangeId)
	{
		if(this.shortListedDrafts==null) return null;
		
		for(DraftExchange de:this.shortListedDrafts)
		{
			if(de.getId().equals(draftExchangeId))
			{
				return de;
			}
		}
		
		return null;
	}
	

	public void addScreenplayTemplates(String screenplayTemplateId)
	{
		if(this.screenplayTemplates==null) this.screenplayTemplates = new ArrayList<String> ();
		this.screenplayTemplates.add(screenplayTemplateId);
	}
	
	public void deleteScreenplayTemplates(String screenplayTemplateId)
	{
		if(this.screenplayTemplates==null) return;
		String toBeDeleted =null;
		for(String stid : this.screenplayTemplates)
		{
			if(stid.equals(screenplayTemplateId))
			{
				toBeDeleted = stid;
				
				break;
			}
		}
		
		this.screenplayTemplates.remove(toBeDeleted);
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getShowName() {
		return showName;
	}

	public void setShowName(String showName) {
		this.showName = showName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocationLine1() {
		return locationLine1;
	}

	public void setLocationLine1(String locationLine1) {
		this.locationLine1 = locationLine1;
	}

	public String getLocationLine2() {
		return locationLine2;
	}

	public void setLocationLine2(String locationLine2) {
		this.locationLine2 = locationLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public IndustryArea getType() {
		return type;
	}

	public void setType(IndustryArea type) {
		this.type = type;
	}

	public String getDirectPhone() {
		return directPhone;
	}

	public void setDirectPhone(String directPhone) {
		this.directPhone = directPhone;
	}

	public String getDirectFax() {
		return directFax;
	}

	public void setDirectFax(String directFax) {
		this.directFax = directFax;
	}

	public String getiMDBLink() {
		return iMDBLink;
	}

	public void setiMDBLink(String iMDBLink) {
		this.iMDBLink = iMDBLink;
	}

	public String getIndustryReferences() {
		return industryReferences;
	}

	public void setIndustryReferences(String industryReferences) {
		this.industryReferences = industryReferences;
	}

	public List<DraftExchange> getShortListedDrafts() {
		return shortListedDrafts;
	}

	public void setShortListedDrafts(List<DraftExchange> shortListedDrafts) {
		this.shortListedDrafts = shortListedDrafts;
	}


	public List<String> getScreenplayTemplates() {
		return screenplayTemplates;
	}


	public void setScreenplayTemplates(List<String> screenplayTemplates) {
		this.screenplayTemplates = screenplayTemplates;
	}
	
	
	
}
