package com.easydraft.app.beans;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author sunbin
 *
 */
@Document
public class DraftExchange {

	@Id
	private String Id;
	
	private String mainAuthorId;
	
	private String draftId;
	
	private String entertainmentProfessionalId;
	
	private int authroizationLevel;
	
	private HotLevel levelOfInterest;
	
	private Map<String,String> comments;
	
	public static final int AUTHORIZED = 100;
	
	public enum HotLevel{
		Light_Interesting, Moderate_Interesting, Very_Interesting
	}

	
	public DraftExchange() {
		super();
	}

	public DraftExchange(String id, String mainAuthorId, String draftId,
			String entertainmentProfessionalId, int authroizationLevel,
			HotLevel levelOfInterest, Map<String, String> comments) {
		super();
		Id = id;
		this.mainAuthorId = mainAuthorId;
		this.draftId = draftId;
		this.entertainmentProfessionalId = entertainmentProfessionalId;
		this.authroizationLevel = authroizationLevel;
		this.levelOfInterest = levelOfInterest;
		this.comments = comments;
	}

	public void generateId()
	{
		this.Id = this.mainAuthorId+this.draftId;
	}
	
	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getMainAuthorId() {
		return mainAuthorId;
	}

	public void setMainAuthorId(String mainAuthorId) {
		this.mainAuthorId = mainAuthorId;
	}

	public String getDraftId() {
		return draftId;
	}

	public void setDraftId(String draftId) {
		this.draftId = draftId;
	}

	public String getEntertainmentProfessionalId() {
		return entertainmentProfessionalId;
	}

	public void setEntertainmentProfessionalId(String entertainmentProfessionalId) {
		this.entertainmentProfessionalId = entertainmentProfessionalId;
	}

	public int getAuthroizationLevel() {
		return authroizationLevel;
	}

	public void setAuthroizationLevel(int authroizationLevel) {
		this.authroizationLevel = authroizationLevel;
	}

	public HotLevel getLevelOfInterest() {
		return levelOfInterest;
	}

	public void setLevelOfInterest(HotLevel levelOfInterest) {
		this.levelOfInterest = levelOfInterest;
	}

	public Map<String, String> getComments() {
		return comments;
	}

	public void setComments(Map<String, String> comments) {
		this.comments = comments;
	}
	
	
}
