package com.easydraft.app.beans;

import java.security.MessageDigest;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author sunbin
 *
 */
@Document
public class DraftPreview {
	@Id
	private String id;
	private String previewName;
	private String previewGenerationURL;
	private String previewURL;
	
	
	public DraftPreview() {
		super();
	}


	public DraftPreview(String id, String previewName,
			String previewGenerationURL, String previewURL) {
		super();
		this.id = id;
		this.previewName = previewName;
		this.previewGenerationURL = previewGenerationURL;
		this.previewURL = previewURL;
	}

	public void generateId(String user_id)
	{
		String idToBeEncoded = user_id+System.currentTimeMillis();
	
		try{
			
			byte[] byteIdToBeEncoded = idToBeEncoded.getBytes("UTF-8");

			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			
			byte[] md = mdInst.digest(byteIdToBeEncoded);
			
			//byte[] to hex-char[] -> 1 byte to 2 digits hex number
			char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
			int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
			
			this.id = new String(str);
			
		}catch(Exception e)
		{
			e.printStackTrace();
			
			this.id = new String(idToBeEncoded);
		}
	}
	
	
	public void generatePreviewGenerationURL()
	{
		this.previewGenerationURL=this.previewName.replace(" ", "_")+"-previewEdit-"+System.currentTimeMillis();
	}
	
	public void generatePreviewURL()
	{
		this.previewURL=this.previewName.replace(" ", "_")+"-preview-"+System.currentTimeMillis();
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getPreviewName() {
		return previewName;
	}


	public void setPreviewName(String previewName) {
		this.previewName = previewName;
	}


	public String getPreviewGenerationURL() {
		return previewGenerationURL;
	}


	public void setPreviewGenerationURL(String previewGenerationURL) {
		this.previewGenerationURL = previewGenerationURL;
	}


	public String getPreviewURL() {
		return previewURL;
	}


	public void setPreviewURL(String previewURL) {
		this.previewURL = previewURL;
	}
	
}