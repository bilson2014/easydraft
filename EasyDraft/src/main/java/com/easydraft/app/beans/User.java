package com.easydraft.app.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author sunbin
 *
 */
@Document
public class User implements Serializable {
	@Id
	private String id;
	private String showName;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private String locationLine1;
	private String locationLine2;
	private String city;
	private Country country;
	private String postCode;
	private String history;
	private Boolean confirmation;
	private List<Draft> drafts;
	
	public User() {
		super();
	}


	public User(String id, String showName, String password, String firstName,
			String lastName, String email, String locationLine1,
			String locationLine2, String city, Country country,
			String postCode, String history, Boolean confirmation,
			List<Draft> drafts) {
		super();
		this.id = id;
		this.showName = showName;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.locationLine1 = locationLine1;
		this.locationLine2 = locationLine2;
		this.city = city;
		this.country = country;
		this.postCode = postCode;
		this.history = history;
		this.confirmation = confirmation;
		this.drafts = drafts;
	}

	
	public List<Draft> getSelfCreatedDrafts() {
		
		if(this.drafts==null) return null;
		List<Draft> scd = new ArrayList<Draft>();
		for(Draft d : this.drafts)
		{
			if(d.getMainAuthorId().equals(this.id))
			{
				scd.add(d);
			}
		}
		
		return scd;
	}
	
	public List<Draft> getCoAuthoredDrafts() {
		
		if(this.drafts==null) return null;
		List<Draft> cad = new ArrayList<Draft>();
		for(Draft d : this.drafts)
		{
			if(!d.getMainAuthorId().equals(this.id))
			{
				cad.add(d);
			}
		}
		
		return cad;
	}
	
	public List<Draft> getDrafts() {
		return drafts;
	}

	public void setDrafts(List<Draft> createdDraft) {
		this.drafts = createdDraft;
	}

	public void addDraft(Draft d)
	{
		if(this.drafts==null) this.drafts=new ArrayList<Draft>();
		this.drafts.add(d);
	}
	
	public void deleteDraft(Draft d)
	{
		if(this.drafts==null) return;
		this.drafts.remove(d);
	}
	
	public void deleteDraftById(String draft_id)
	{
		if(this.drafts==null) return;
		
		Draft toBeDeleted = null;
		
		for(Draft d : this.drafts)
		{
			if(d.getId().equals(draft_id))
			{
				toBeDeleted = d;
				
				break;
			}
		}
		
		this.deleteDraft(toBeDeleted);
	}
	
	public void deleteDraftWithTitle(String title)
	{
		if(this.drafts==null) return;
		
		for(Draft d:this.drafts)
		{
			if(d.getTitle().equals(title))
			{
				this.drafts.remove(d);
			}
		}
	}
	
	public Draft searchDraftWithTitle(String title)
	{
		if(this.drafts==null) return null;
		
		for(Draft d:this.drafts)
		{
			if(d.getTitle().equals(title))
			{
				return d;
			}
		}
		return null;
	}
	
	public Draft searchDraftById(String id){
		
		if(this.drafts==null) return null;
		
		for(Draft d : this.drafts)
		{
			if(d.getId().equals(id))
			{
				return d;
			}
		}
		
		return null;
	}

	public Boolean getConfirmation() {
		return confirmation;
	}


	public void setConfirmation(Boolean confirmation) {
		this.confirmation = confirmation;
	}


	public enum Country{
		China, United_Kingdom, United_States
	}

	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public Country getCountry() {
		return country;
	}


	public void setCountry(Country country) {
		this.country = country;
	}


	public String getPostCode() {
		return postCode;
	}


	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShowName() {
		return showName;
	}
	public void setShowName(String showName) {
		this.showName = showName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLocationLine1() {
		return locationLine1;
	}
	public void setLocationLine1(String locationLine1) {
		this.locationLine1 = locationLine1;
	}
	public String getLocationLine2() {
		return locationLine2;
	}
	public void setLocationLine2(String locationLine2) {
		this.locationLine2 = locationLine2;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	
	public String toString()
	{
		return this.firstName + " " + this.lastName;
	}
}
