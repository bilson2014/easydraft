package com.easydraft.app.beans;

public class ScreenplayTemplateJSON {
	private String pre;
	private String title;
	private String preHtml;
	private String html;
	public ScreenplayTemplateJSON(String pre, String title, String preHtml,
			String html) {
		super();
		this.pre = pre;
		this.title = title;
		this.preHtml = preHtml;
		this.html = html;
	}
	public ScreenplayTemplateJSON() {
		super();
	}
	public ScreenplayTemplateJSON(ScreenplayTemplate st)
	{
		super();
		this.pre = st.getImageURL();
		this.title = st.getTitle();
		this.preHtml = "<h1>"+st.getTitle() + "</h1><br/>" +st.getPreContents();
		this.html = st.getContents();
	}
	public String getPre() {
		return pre;
	}
	public void setPre(String pre) {
		this.pre = pre;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPreHtml() {
		return preHtml;
	}
	public void setPreHtml(String preHtml) {
		this.preHtml = preHtml;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	
	
}
