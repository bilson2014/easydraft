package com.easydraft.app.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author sunbin
 *
 */
@Document
public class DraftProduction implements Serializable {

	private String pitch;
	
	private String logline;
	
	private Genres genre;
	
	private String exposedPreviewId;
	
	private List<DraftPreview> allPreviews;
	
	public enum Genres{
		Action, Adventure, Comedy, Crime, Drama, Historical, Horror, Musical, Science_Fiction, War, Western
	}

	public DraftProduction() {
		super();
	}


	public DraftProduction(String pitch, String logline, Genres genre,
			String exposedPreviewId, List<DraftPreview> allPreviews) {
		super();
		this.pitch = pitch;
		this.logline = logline;
		this.genre = genre;
		this.exposedPreviewId = exposedPreviewId;
		this.allPreviews = allPreviews;
	}


	public void addPreview(DraftPreview preview) {
		if(this.allPreviews==null) this.allPreviews=new ArrayList<DraftPreview>();
		this.allPreviews.add(preview);
	}
	
	public void addPreview(String previewName, String previewGenerationURL, String previewURL){
		if(this.allPreviews==null) this.allPreviews=new ArrayList<DraftPreview>();
		DraftPreview p = new DraftPreview();
		p.setPreviewName(previewName);
		p.setPreviewGenerationURL(previewGenerationURL);
		p.setPreviewURL(previewURL);
		this.allPreviews.add(p);
	}
	
	public void deletePreviewByName(String previewName){
		if(this.allPreviews==null) return;
		for (DraftPreview p:this.allPreviews)
		{
			if(p.getPreviewName().equals(previewName))
			{
				this.allPreviews.remove(p);
			}
		}
	}
	
	public void deletePreviewById(String id){
		if(this.allPreviews==null) return;
		
		DraftPreview toBeDeleted=null;
		for (DraftPreview p:this.allPreviews)
		{
			if(p.getId().equals(id))
			{
				toBeDeleted=p;
				
				break;
			}
		}
		this.deletePreview(toBeDeleted);
	}
	
	public void deletePreview(DraftPreview dp)
	{
		if(this.allPreviews==null) return;
		this.allPreviews.remove(dp);
	}
	
	public DraftPreview searchPreviewByName(String previewName){
		if(this.allPreviews==null) return null;
		
		for (DraftPreview p:this.allPreviews)
		{
			if(p.getPreviewName().equals(previewName))
			{
				return p;
			}
		}
		
		return null;
	}
	
	public DraftPreview searchPreviewById(String id){
		if(this.allPreviews==null) return null;
		
		for (DraftPreview p:this.allPreviews)
		{
			if(p.getId().equals(id))
			{
				return p;
			}
		}
		
		return null;
	}
	
	
	public String getExposedPreviewId() {
		return exposedPreviewId;
	}


	public void setExposedPreviewId(String exposedPreviewId) {
		this.exposedPreviewId = exposedPreviewId;
	}


	public Genres getGenre() {
		return genre;
	}


	public void setGenre(Genres genre) {
		this.genre = genre;
	}


	public String getLogline() {
		return logline;
	}

	public void setLogline(String logline) {
		this.logline = logline;
	}

	public String getPitch() {
		return pitch;
	}

	public void setPitch(String pitch) {
		this.pitch = pitch;
	}

	public List<DraftPreview> getAllPreviews() {
		return allPreviews;
	}

	public void setAllPreviews(List<DraftPreview> allPreviews) {
		this.allPreviews = allPreviews;
	}
	
}


