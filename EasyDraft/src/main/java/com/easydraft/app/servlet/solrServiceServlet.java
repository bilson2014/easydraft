package com.easydraft.app.servlet;

import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;

public class solrServiceServlet extends GenericServlet {

	
	@Override
	public void init() throws ServletException{
		
		/*
		//Embedded Solr Server
		System.setProperty("solr.solr.home", "E:\\Project\\workspace\\solr");
		String solrDir = "E:\\Project\\workspace\\solr";
		//CoreContainer.Initializer initializer = new CoreContainer.Initializer();
		//CoreContainer coreContainer = initializer.initialize();
		CoreContainer coreContainer = new CoreContainer(solrDir);
		coreContainer.load();
		SolrServer server = new EmbeddedSolrServer(coreContainer, "");*/
		
		
		//Http Solr Server
		String url = "http://localhost:8983/solr";
		SolrServer server = new HttpSolrServer(url);
				
	}
	
	
	@Override
	public void service(ServletRequest req, ServletResponse res)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

}
