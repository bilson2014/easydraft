package com.easydraft.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.easydraft.app.beans.EntertainmentProfessional;
import com.easydraft.app.beans.ScreenplayTemplate;
import com.easydraft.app.beans.ScreenplayTemplateJSON;
import com.easydraft.app.services.ScreenplayTemplateManagementService;
import com.easydraft.app.util.Encoding;

/**
 * @author sunbin
 *
 */
@Controller
public class ScreenplayTemplateController {
	
	@Autowired
	MongoTemplate userMongoTemplate;
	@Autowired
	MongoDbFactory easyDraftDBFactory;
	
	private static final Logger logger = LoggerFactory.getLogger(UserCollaborationController.class);
	
	
	@RequestMapping(value="/loadScreenplayTemplates", method = RequestMethod.GET)
	public @ResponseBody Map<String,List<ScreenplayTemplateJSON>> loadScreenplayTemplateAjax(HttpServletRequest req)
	{
		ScreenplayTemplateManagementService stms = new ScreenplayTemplateManagementService(userMongoTemplate);
		
		List<ScreenplayTemplate> screenplayTemplates = stms.listAllPublicScreenplayTemplates();
		
		List<ScreenplayTemplateJSON> screenplayTemplatesJSONRaw = new ArrayList<ScreenplayTemplateJSON>();
		
		
		for(ScreenplayTemplate st :screenplayTemplates)
		{
			screenplayTemplatesJSONRaw.add(new ScreenplayTemplateJSON(st));
		}
		
		Map<String,List<ScreenplayTemplateJSON>> responseScreenplayTemplatesJSON = new HashMap<String,List<ScreenplayTemplateJSON>>();
		
		responseScreenplayTemplatesJSON.put("templates",screenplayTemplatesJSONRaw);
		
		/*
		JSONArray screenplayTemplatesJSON = JSONArray.fromObject(screenplayTemplatesJSONRaw);
		
		JSONObject screenplayTemplatesJSONObject = new JSONObject();
		
		screenplayTemplatesJSONObject.put("templates", screenplayTemplatesJSON);
		
		System.out.println("JSON STRING = " + screenplayTemplatesJSON.toString());
		
		model.addAttribute("screenplayTemplatesJSON", screenplayTemplatesJSON);
		
		model.addAttribute("screenplayTemplatesJSONStr", screenplayTemplatesJSON.toString());
		
		model.addAttribute("screenplayTemplatesJSONObject", screenplayTemplatesJSONObject);
		
		model.addAttribute("screenplayTemplatesJSONObjectStr", screenplayTemplatesJSONObject.toString());*/
		
		return responseScreenplayTemplatesJSON;
	}
	
	@RequestMapping(value = "/screenplaytemplate", method = RequestMethod.GET)
	public String editScreenplayTemplate(@RequestParam(value="action")String action, @RequestParam(value="screenplaytemplate",required=false)String screenplayTemplateId, Locale locale, Model model, HttpServletRequest req)
	{
		logger.info("Welcome screenplayTemplate! The client locale is {}.", locale);
		
		ScreenplayTemplateManagementService stms = new ScreenplayTemplateManagementService(userMongoTemplate);
		
		EntertainmentProfessional login_ep = (EntertainmentProfessional)req.getSession().getAttribute("login-ep");
		
		String logStr = "[SCREENPLAYTEMPLATE ACTION="+action+" & ENTERTAINMENTPROFESSIONAL="+login_ep.getId()+"] ";
		
		if(action.equals("new") && (screenplayTemplateId==null || screenplayTemplateId.equals("")))
		{
			logger.info(logStr+"Entertainment Professional <" + login_ep.getShowName()+">" + " create the screenplay template");
			
			String newScreenplayTemplateId = stms.createScreenplayTemplate(login_ep.getId(), null, null, null, null);
			
			System.out.println(logStr+" Entertainment Professional "+login_ep.getId()+" has created screenplay template "+newScreenplayTemplateId);
			
			logger.info(logStr+" Entertainment Professional "+login_ep.getId()+" has created screenplay template "+newScreenplayTemplateId);
			
			return "redirect:/screenplaytemplate?action=new&screenplaytemplate="+newScreenplayTemplateId;
		}
		else if(action.equals("delete"))
		{
			logger.info(logStr+"Entertainment Professional <" + login_ep.getShowName()+">" + " delete the screenplay template id="+screenplayTemplateId);
			
			stms.deleteScreenplayTemplate(screenplayTemplateId);
			
			System.out.println(logStr+" Entertainment Professional "+login_ep.getId()+" has created screenplay template "+screenplayTemplateId);
			
			logger.info(logStr+" Entertainment Professional "+login_ep.getId()+" has created screenplay template "+screenplayTemplateId);
			
			return "redirect:/ephome";
		}
		else if(action.equals("publicize"))
		{
			logger.info(logStr+"Entertainment Professional <" + login_ep.getShowName()+">" + " publicize the screenplay template id="+screenplayTemplateId);
			
			stms.publicizeScreenplayTemplate(screenplayTemplateId);
			
			return "redirect:/ephome";
		}
		else if(action.equals("unpublicize"))
		{
			logger.info(logStr+"Entertainment Professional <" + login_ep.getShowName()+">" + " unpublicize the screenplay template id="+screenplayTemplateId);
			
			stms.unpublicizeScreenplayTemplate(screenplayTemplateId);
			
			return "redirect:/ephome";
		}
		else 
		{
			logger.info(logStr+"Entertainment Professional <" + login_ep.getShowName()+">" + " edit the screenplay template id="+screenplayTemplateId);
		
			ScreenplayTemplate st = stms.findScreenplayTemplateById(screenplayTemplateId);
			
			if(st==null)
			{
				return "redirect:/ephome";
			}
			else
			{
				model.addAttribute("screenplaytemplate", st);
				
				model.addAttribute("contents", st.getContents());
				
				logger.info(logStr+"Found screenplay template "+screenplayTemplateId);
				
				return "steditor";
			}
		}
	}
	
	
	@RequestMapping(value = "/screenplaytemplate", method = RequestMethod.POST, params = "save")
	public String saveAction (@RequestParam(value="screenplaytemplate")String screenplayTemplateId, @ModelAttribute("screenplaytemplate")ScreenplayTemplate screenplaytemplate, Locale locale, Model model, HttpServletRequest req){
		
		String mycontent = req.getParameter("myContent");
		
		String contents = Encoding.transEncodingFromPage(mycontent);
		
		String title = Encoding.transEncodingFromPage(screenplaytemplate.getTitle());
		
		String imageURL = Encoding.transEncodingFromPage(screenplaytemplate.getImageURL());
		
		String preContents = Encoding.transEncodingFromPage(screenplaytemplate.getPreContents());
		
		ScreenplayTemplateManagementService stms = new ScreenplayTemplateManagementService(userMongoTemplate);
		
		stms.updateExistingScreenplayTemplate(screenplayTemplateId, title, imageURL, preContents, contents);
		
		System.out.println("[SCREENPLAYTEMPLATE] Updated the screenplay template "+ screenplayTemplateId + " (TITLE=" + title +"; IMAGE_URL="+imageURL+"; PRECONENTS="+preContents+"; CONTENTS="+contents+") ");
		
		logger.info("[SCREENPLAYTEMPLATE] Updated the screenplay template "+ screenplayTemplateId + " (TITLE=" + title +"; IMAGE_URL="+imageURL+"; PRECONENTS="+preContents+"; CONTENTS="+contents+") ");
		
		return "redirect:/screenplaytemplate?action=edit&screenplaytemplate="+screenplayTemplateId; 
	}
	
	@RequestMapping(value = "/screenplaytemplate", method = RequestMethod.POST, params = "close")
	public String close (@RequestParam(value="action")String action, @RequestParam(value="screenplaytemplate")String screenplayTemplateId, Locale locale, Model model, HttpServletRequest req) 
	{
		if(action.equals("new"))
		{
			ScreenplayTemplateManagementService stms = new ScreenplayTemplateManagementService(userMongoTemplate);
			
			stms.deleteScreenplayTemplate(screenplayTemplateId);
			
			System.out.println("[SCREENPLAYTEMPLATE] Deleted the screenplay template "+ screenplayTemplateId);
			
			logger.info("[SCREENPLAYTEMPLATE] Deleted the screenplay template "+ screenplayTemplateId);
			
			return "redirect:/ephome";
			
			
		}else
		{
			return "redirect:/ephome";
		}
	}
	

}
