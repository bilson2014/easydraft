package com.easydraft.app.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.User;
import com.easydraft.app.services.DraftManagementService;
import com.easydraft.app.services.UserCollaborationService;
import com.easydraft.app.services.UserManagementService;
import com.easydraft.app.util.Encoding;
/**
 * Handles requests for the listcollaborators and selectwriter page.
 */
/**
 * @author sunbin
 *
 */
@Controller
public class UserCollaborationController {

	@Autowired
	MongoTemplate userMongoTemplate;
	@Autowired
	MongoDbFactory easyDraftDBFactory;

	private static final Logger logger = LoggerFactory.getLogger(UserCollaborationController.class);
	
	
	@RequestMapping(value = "/listcollaborators", method = RequestMethod.GET)
	public String listAllCollaborators(@RequestParam(value="draft")String draft_id, @RequestParam(value="user")String user_id, Locale locale, Model model, HttpServletRequest req)
	{
		UserCollaborationService ucs = new UserCollaborationService(userMongoTemplate,easyDraftDBFactory);
		
		List<User> collaboratedWriters = ucs.findAllCollaborators(draft_id, user_id);
		
		List<Draft> allCollaboratedDrafts = ucs.findAllCollaboratedDrafts(draft_id, collaboratedWriters);
		
		model.addAttribute("writers", collaboratedWriters);
		
		model.addAttribute("collaboratedDrafts",allCollaboratedDrafts);
		
		UserManagementService ums = new UserManagementService(userMongoTemplate);
		
		User currentUser = ums.findUserById(user_id);
		
		Draft currentDraft = currentUser.searchDraftById(draft_id);
		
		model.addAttribute("currentDraft", currentDraft);
		
		model.addAttribute("userId", user_id);
		
		return "listcollaborators";
	}
	
	
	@RequestMapping(value = "/selectwriter", method = RequestMethod.GET)
	public String listAllOtherWriters(@RequestParam(value="draft")String draft_id, @RequestParam(value="user")String user_id, Locale locale, Model model, HttpServletRequest req)
	{
		UserCollaborationService ucs = new UserCollaborationService(userMongoTemplate,easyDraftDBFactory);
		
		List<User> otherWriters = ucs.findAllOtherWriters(draft_id, user_id);
		
		model.addAttribute("otherWriters", otherWriters);
		
		model.addAttribute("draft_id", draft_id);
		
		model.addAttribute("originalWriter", user_id);
		
		return "selectwriter";
	}
	
	@RequestMapping(value = "/addnewcollaborator", method = RequestMethod.GET)
	public String addNewCollaborator(@RequestParam(value="draft")String draft_id, @RequestParam(value="user")String added_user, @RequestParam(value="origin")String original_user, Locale locale, Model model, HttpServletRequest req)
	{
		UserCollaborationService ucs = new UserCollaborationService(userMongoTemplate,easyDraftDBFactory);
		
		ucs.addCollaboratorToDraft(draft_id, added_user, original_user);
		
		return "redirect:/selectwriter?draft="+draft_id+"&user="+original_user;
	}
	
	@RequestMapping(value = "/removecollaborator",method = RequestMethod.GET)
	public String removeCollaborator(@RequestParam(value="draft")String draft_id, @RequestParam(value="user")String user_id, @RequestParam(value="maindraft", required=false)String main_draft_id,Locale locale, Model model, HttpServletRequest req)
	{
		UserCollaborationService ucs = new UserCollaborationService(userMongoTemplate,easyDraftDBFactory);
		
		ucs.removeCollaborator(draft_id, user_id);
		
		User login_user = (User)req.getSession().getAttribute("login-user");
		
		if(user_id.equals(login_user.getId())) 	//remove self
			return "redirect:/home";
		else 									//remove other collaborator
			return "redirect:./listcollaborators?draft="+main_draft_id+"&user="+login_user.getId();
	}
	
	@RequestMapping(value = "/draftviewer", method = RequestMethod.GET)
	public String viewDraftProgress(@RequestParam(value="details")String details, @RequestParam(value="draft")String draft_id, @RequestParam(value="user")String user_id, Locale locale, Model model, HttpServletRequest req, HttpServletResponse response)
	{
		
		DraftManagementService dms = new DraftManagementService(userMongoTemplate,easyDraftDBFactory);
		UserManagementService ums = new UserManagementService(userMongoTemplate);
		
		ByteArrayOutputStream contentOutStream = new ByteArrayOutputStream();
		
		User author = ums.findUserById(user_id);
		
		Draft draft = dms.getExistingDraft(draft_id, author, contentOutStream);
		
		System.out.println("Reading file - " + draft.getExternalURL());
		
		String contents = Encoding.transEncodingFromDB(contentOutStream);
		
		System.out.println("Content of file ["+draft.getExternalURL()+"] is "+contents);
		
		//save it as a html file.
		response.setContentType("text/html;charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String prefix = "<html><head></head><body>";
		String suffix = "</body></html>";
		
		try {
			response.getWriter().print(prefix);
			response.getWriter().print(contents);
			response.getWriter().print(suffix);
			response.getWriter().flush();
		}catch (IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
}
