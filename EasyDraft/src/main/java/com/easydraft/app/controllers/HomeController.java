package com.easydraft.app.controllers;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.DraftExchange;
import com.easydraft.app.beans.EntertainmentProfessional;
import com.easydraft.app.beans.ScreenplayTemplate;
import com.easydraft.app.beans.User;
import com.easydraft.app.services.ScreenplayTemplateManagementService;
import com.easydraft.app.services.UserManagementService;

/**
 * Handles requests for the application home page.
 */
/**
 * @author sunbin
 *
 */
@Controller
public class HomeController {
	
	@Autowired
	MongoTemplate userMongoTemplate;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest req) {
		logger.info("Welcome back, writer! The client locale is {}.", locale);
		
		User login_user = (User)req.getSession().getAttribute("login-user");
		
		if(login_user!=null)
		{
			UserManagementService ums = new UserManagementService(userMongoTemplate);
			
			User user = ums.findUserById(login_user.getId());
			
			List<Draft> selfCreatedDrafts = user.getSelfCreatedDrafts();

			List<Draft> coAuthoredDrafts = user.getCoAuthoredDrafts();
			
			model.addAttribute("user", user);
			
			model.addAttribute("locale", locale);
			
			model.addAttribute("selfCreatedDrafts" , selfCreatedDrafts);
			
			model.addAttribute("coAuthoredDrafts" , coAuthoredDrafts);
			
			return "home";
		}
		else
		{
			return "redirect:/login";
		}
	}
	
	@RequestMapping(value="/ephome", method = RequestMethod.GET)
	public String ephome(@RequestParam(value="result",required=false)String result, Locale locale, Model model, HttpServletRequest req) {
		logger.info("Welcome back, entertaiment professional! The client locale is {}.", locale);
		
		EntertainmentProfessional login_ep = (EntertainmentProfessional)req.getSession().getAttribute("login-ep");
		
		if(login_ep!=null)
		{
			UserManagementService ums = new UserManagementService(userMongoTemplate);
			
			EntertainmentProfessional ep = ums.findEntertainmentProfessionalById(login_ep.getId());
			
			List<DraftExchange> interestList = ep.getInterestList();
			
			List<DraftExchange> purchasedList = ep.getPurchasedList();
			
			List<Draft> interestDrafts = new ArrayList<Draft>();
			
			List<User> interestAuthor = new ArrayList<User>();
			
			if(interestList!=null)
			{
				for(int i=0;i<interestList.size();i++)
				{
					User user = ums.findUserById(interestList.get(i).getMainAuthorId());
					
					Draft draft = user.searchDraftById(interestList.get(i).getDraftId());
					
					interestDrafts.add(draft);
					
					interestAuthor.add(user);
				}
			}
			
			List<Draft> purchasedDrafts = new ArrayList<Draft>();
			
			List<User> purchasedAuthor = new ArrayList<User>();
			
			if(purchasedList!=null)
			{
				for(int i=0;i<purchasedList.size();i++)
				{
					User user = ums.findUserById(purchasedList.get(i).getMainAuthorId());
					
					Draft draft = user.searchDraftById(purchasedList.get(i).getDraftId());
					
					purchasedDrafts.add(draft);
					
					purchasedAuthor.add(user);
				}
			}
			
			
			
			ScreenplayTemplateManagementService stms = new ScreenplayTemplateManagementService(userMongoTemplate);
			
			List<ScreenplayTemplate> screenplayTemplates = stms.listAllScreenplayTemplatesForEntertainmentProfessional(ep.getId());
			
			
			
			model.addAttribute("ep", ep);
			
			model.addAttribute("locale", locale);
			
			model.addAttribute("interestList",interestList);
			
			model.addAttribute("purchasedList",purchasedList);
			
			model.addAttribute("interestDrafts",interestDrafts);
			
			model.addAttribute("purchasedDrafts",purchasedDrafts);
			
			model.addAttribute("interestAuthor", interestAuthor);
			
			model.addAttribute("purchasedAuthor", purchasedAuthor);
			
			model.addAttribute("screenplayTemplates", screenplayTemplates);
			
			if(result!=null)
				model.addAttribute("result", result);
			
			return "ephome";
		}
		else
		{
			return "redirect:/login";
		}
	}
	
}
