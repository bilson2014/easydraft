package com.easydraft.app.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.ScreenplayTemplate;
import com.easydraft.app.beans.ScreenplayTemplateJSON;
import com.easydraft.app.beans.User;
import com.easydraft.app.services.DraftManagementService;
import com.easydraft.app.services.ScreenplaySearchingServiceImpl;
import com.easydraft.app.services.ScreenplayTemplateManagementService;
import com.easydraft.app.util.Encoding;

/**
 * Handles requests for the drafteditor page.
 */
/**
 * @author sunbin
 *
 */
@Controller
public class DraftEditorController {

	
	@Autowired
	MongoTemplate userMongoTemplate;
	@Autowired
	MongoDbFactory easyDraftDBFactory;
	@Autowired
	ScreenplaySearchingServiceImpl screenplaySearchingServiceImpl;
	
	private static final Logger logger = LoggerFactory.getLogger(DraftEditorController.class);
	
	@RequestMapping(value = "/drafteditor", method = RequestMethod.GET)
	public String editDraft(@RequestParam(value="action")String action, @RequestParam(value="draft",required=false)String draftId, Locale locale, Model model, HttpServletRequest req, HttpServletResponse response) {
		
		logger.info("Welcome drafteditor! The client locale is {}.", locale);
		
		DraftManagementService dms = new DraftManagementService(userMongoTemplate,easyDraftDBFactory);
		
		User author = (User) req.getSession().getAttribute("login-user");
		
		String logStr = "[DRAFTEDITOR ACTION="+action+" & DRAFT="+draftId+" & AUTHOR="+author.getId()+"] ";
		
		if(action.equals("new") && (draftId==null || draftId.equals("")))
		{
			
			logger.info(logStr+"User <" + author.getShowName()+">" + " created the draft");
			
			//all attributes will be initialized when the user perform the save action
			String newDraftId = dms.CreateNewDraft(author);
		
			System.out.println(logStr+"Created draft "+newDraftId+" for user "+author.getId());
			
			logger.info(logStr+"Created draft "+newDraftId+" for user "+author.getId());
			
			return "redirect:/drafteditor?action=new&draft="+newDraftId;
			
		}
		if(action.equals("delete"))
		{
			boolean result = dms.deleteDraft(draftId, author);
			
			if(result)
			{
				//delete production index
				screenplaySearchingServiceImpl.deleteScreenplayProductionbyId(draftId);
			}
			
			System.out.println(logStr+"Deleted draft "+draftId+" for user "+author.getId());
			
			logger.info(logStr+"Deleted draft "+draftId+" for user "+author.getId());
			
			return "redirect:/home";
		}
		if(action.equals("download"))
		{
			ByteArrayOutputStream contentOutStream = new ByteArrayOutputStream();
			
			Draft draft = dms.getExistingDraft(draftId, author, contentOutStream);
			
			String contents = Encoding.transEncodingFromDB(contentOutStream);
			
			System.out.println(logStr+"Downloading draft "+draftId+" for user "+author.getId()+ "( EXTERNAL_URL="+draft.getExternalURL()+"; TITLE = "+draft.getTitle()+"; CONTENT = "+contents+")");
			
			logger.info(logStr+"Downloading draft "+draftId+" for user "+author.getId()+ "( EXTERNAL_URL="+draft.getExternalURL()+"; TITLE = "+draft.getTitle()+"; CONTENT = "+contents+")");
			
			//save it as a html file.
			response.setContentType("text/html;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			
			String prefix = "<html><head><title>"+draft.getTitle()+"</title></head><body>";
			String suffix = "</body></html>";
			
			try {
				response.getWriter().print(prefix);
				response.getWriter().print(contents);
				response.getWriter().print(suffix);
				response.getWriter().flush();
			}catch (IOException e) {
				e.printStackTrace();
			}
			
			return null;
		}
		else 
		{
			
			ByteArrayOutputStream contentOutStream = new ByteArrayOutputStream();
			
			Draft draft = dms.getExistingDraft(draftId, author, contentOutStream);
			
			if(draft==null)
			{
				logger.error("Either login-user or the draft is not found [user.id="+ author.getId() +"; draft.id="+draftId+"]");
				
				draft = new Draft();
			}
			
			model.addAttribute("draft",draft);
			
			String content = Encoding.transEncodingFromDB(contentOutStream);
			
			model.addAttribute("contents",content);
			

			System.out.println(logStr+"Found the draft " + draftId +" for user "+ author.getId() + "( EXTERNAL_URL="+draft.getExternalURL()+"; TITLE = "+draft.getTitle()+"; CONTENT = "+content+")");
			
			logger.info(logStr+"Found the draft " + draftId +" for user "+ author.getId() + "( EXTERNAL_URL="+draft.getExternalURL()+"; TITLE = "+draft.getTitle()+"; CONTENT = "+content+")");
			
			
			/*
			User mainAuthor = ums.findUserById(draft.getMainAuthorId());
			
			User lastEditor = ums.findUserById(draft.getLastEditorId());
			
			model.addAttribute("mainAuthor", mainAuthor);
			
			model.addAttribute("lastEditor", lastEditor);
			*/
			return "drafteditor";
		}
		
	}
	
	@RequestMapping(value = "/drafteditor", method = RequestMethod.POST, params = "save")
	public String saveAction (@RequestParam(value="draft")String draftId, @ModelAttribute("draft")Draft draft, Locale locale, Model model, HttpServletRequest req) {
		
			String mycontent = req.getParameter("myContent");
		
			String content = Encoding.transEncodingFromPage(mycontent);
			
			String title = Encoding.transEncodingFromPage(draft.getTitle());
			
			DraftManagementService dms = new DraftManagementService(userMongoTemplate,easyDraftDBFactory);
			
			User editor = (User) req.getSession().getAttribute("login-user");
			
			dms.UpdateExistingDraft(draftId, title, content, editor);
			
			System.out.println("[DRAFTEDITOR SAVE] Updated the draft "+ draftId + " for user; TITLE=" + title +"; CONTENT="+content);
			
			logger.info("[DRAFTEDITOR SAVE] Updated the draft "+ draftId + " for user; TITLE=" + title +"; CONTENT="+content);
			
			return "redirect:/drafteditor?action=update&draft="+draftId;
		
	}
	
	@RequestMapping(value = "/drafteditor", method = RequestMethod.POST, params = "close")
	public String close (@RequestParam(value="action")String action, @RequestParam(value="draft")String draftId, Locale locale, Model model, HttpServletRequest req) {
		
		if(action.equals("new"))
		{
			User editor = (User) req.getSession().getAttribute("login-user");
			
			DraftManagementService dms = new DraftManagementService(userMongoTemplate,easyDraftDBFactory);
			
			dms.deleteDraft(draftId, editor);
			
			System.out.println("[DRAFTEDITOR CLOSE] Deleted draft "+draftId+" for user "+editor.getId());
			
			logger.info("[DRAFTEDITOR CLOSE] Deleted draft "+draftId+" for user "+editor.getId());	
			
			return "redirect:/home";
		}
		else
			return "redirect:/home";
		
	}
	
	
}
