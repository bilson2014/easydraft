package com.easydraft.app.controllers;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easydraft.app.beans.EntertainmentProfessional;
import com.easydraft.app.beans.User;
import com.easydraft.app.services.UserManagementService;

/**
 * Handles requests for the application register page.
 */
/**
 * @author sunbin
 *
 */
@Controller
public class RegisterController {

	@Autowired
	MongoTemplate userMongoTemplate;

	private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);
	
	@RequestMapping(value = "/createAccount", method = RequestMethod.GET)
	public String createAccount(Locale locale, Model model, HttpServletRequest req)
	{
		logger.info("Welcome Register! The client locale is {}.", locale);
		
		return "createAccount";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerRender(Locale locale, Model model, HttpServletRequest req) {
		
		logger.info("Welcome Register! The client locale is {}.", locale);
		
		User user = new User();
		
		user.setConfirmation(false);
		
		model.addAttribute("user", user);
		
		model.addAttribute("countries", User.Country.values());
		
		return "register";
	}
	

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerAction (@ModelAttribute("user")User user, Locale locale, Model model, HttpServletRequest req) {
	
		if((user.getShowName()==null) || (user.getPassword()==null) || user.getShowName().isEmpty() || user.getPassword().isEmpty())
		{
			model.addAttribute("alert", "Please provide the required information annotated by *. (login name & password)");
			
			logger.info("Registration failed - invalid user name or password");
			
			return "register";
		}
		else
		{
			UserManagementService ums = new UserManagementService(userMongoTemplate);
			if(ums.register(user))
			{
				logger.info("Registration successful - user created "+user.getShowName());
				
				//re-login
				user = ums.userLogin(user.getShowName(), user.getPassword());
				
				req.getSession().setAttribute("login-user", user);
				
				return "redirect:/home";
			}
			else //duplicate user found
			{
				model.addAttribute("alert", "The name "+user.getShowName()+" has already been used. Please change another name");
			
				logger.info("Registration failed - duplicate user name found");
				
				return "register";
			}
		}
	}
	
	
	@RequestMapping(value = "/apply", method = RequestMethod.GET)
	public String applicationRender(Locale locale, Model model, HttpServletRequest req) {
		
		logger.info("Welcome to apply! The client locale is {}.", locale);
		
		EntertainmentProfessional ep = new EntertainmentProfessional();
		
		model.addAttribute("entertainmentProfessional", ep);
		
		model.addAttribute("countries", EntertainmentProfessional.Country.values());
		
		model.addAttribute("types",EntertainmentProfessional.IndustryArea.values());
		
		return "epapplication";
	}
	
	@RequestMapping(value = "/apply", method = RequestMethod.POST)
	public String applyAction (@ModelAttribute("entertainmentProfessional")EntertainmentProfessional entertainmentProfessional, Locale locale, Model model, HttpServletRequest req) {
	
		if((entertainmentProfessional.getShowName()==null) || (entertainmentProfessional.getPassword()==null) || entertainmentProfessional.getShowName().isEmpty() || entertainmentProfessional.getPassword().isEmpty())
		{
			model.addAttribute("alert", "Please provide the required information annotated by *. (login name & password)");
			
			logger.info("Registration failed - invalid user name or password");
			
			return "epapplication";
		}
		else
		{
			UserManagementService ums = new UserManagementService(userMongoTemplate);
			if(ums.register(entertainmentProfessional))
			{
				logger.info("Registration successful - user created "+entertainmentProfessional.getShowName());
				
				//re-login
				entertainmentProfessional = ums.entertainmentProfessionalLogin(entertainmentProfessional.getShowName(), entertainmentProfessional.getPassword());
				
				req.getSession().setAttribute("login-ep", entertainmentProfessional);
				
				return "redirect:/ephome";
			}
			else //duplicate user found
			{
				model.addAttribute("alert", "The name "+entertainmentProfessional.getShowName()+" has already been used. Please change another name");
			
				logger.info("Registration failed - duplicate user name found");
				
				return "epapplication";
			}
		}
	}
	
}
