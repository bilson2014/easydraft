package com.easydraft.app.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.DraftExchange;
import com.easydraft.app.beans.EntertainmentProfessional;
import com.easydraft.app.beans.User;
import com.easydraft.app.services.DraftExchangeManagementService;
import com.easydraft.app.services.ScreenplaySearchingServiceImpl;
import com.easydraft.app.services.UserManagementService;

@Controller
public class DraftExchangeController {
	@Autowired
	MongoTemplate userMongoTemplate;
	@Autowired
	MongoDbFactory easyDraftDBFactory;
	@Autowired
	ScreenplaySearchingServiceImpl screenplaySearchingServiceImpl;
	
	private static final Logger logger = LoggerFactory.getLogger(DraftExchangeController.class);
	
	@RequestMapping(value="/ephome", method = RequestMethod.POST)
	public String draftSearch(Locale locale, Model model, HttpServletRequest req)
	{
		String searchString = req.getParameter("searchbox");
		
		System.out.println("[EPHOME - EP SEARCH SCREENPLAYS] Search String="+searchString);
		
		if(searchString==null || searchString.equals("")) searchString="*";
		
		List<Map<String,String>> resultList = screenplaySearchingServiceImpl.searchScreenplay(searchString);
		
		List<Draft> resultScreenplayList = new ArrayList<Draft>();
		
		for(Map<String,String> result:resultList)
		{
			String draft_id = result.get("draft_id");
			
			String author_id = result.get("author_id");
			
			UserManagementService ums = new UserManagementService(userMongoTemplate);
			
			User author = ums.findUserById(author_id);
			
			Draft draft = author.searchDraftById(draft_id);
			
			resultScreenplayList.add(draft);
		}
		
		model.addAttribute("searchResult", resultScreenplayList);
		
		EntertainmentProfessional login_ep = (EntertainmentProfessional)req.getSession().getAttribute("login-ep");
		
		if(login_ep!=null)
		{
			UserManagementService ums = new UserManagementService(userMongoTemplate);
			
			EntertainmentProfessional ep = ums.findEntertainmentProfessionalById(login_ep.getId());
			
			List<DraftExchange> interestList = ep.getInterestList();
			
			List<DraftExchange> purchasedList = ep.getPurchasedList();
			
			List<Draft> interestDrafts = new ArrayList<Draft>();
			
			List<User> interestAuthor = new ArrayList<User>();
			
			if(interestList!=null)
			{
				for(int i=0;i<interestList.size();i++)
				{
					User user = ums.findUserById(interestList.get(i).getMainAuthorId());
					
					Draft draft = user.searchDraftById(interestList.get(i).getDraftId());
					
					interestDrafts.add(draft);
					
					interestAuthor.add(user);
				}
			}
			
			List<Draft> purchasedDrafts = new ArrayList<Draft>();
			
			List<User> purchasedAuthor = new ArrayList<User>();
			
			if(purchasedList!=null)
			{
				for(int i=0;i<purchasedList.size();i++)
				{
					User user = ums.findUserById(purchasedList.get(i).getMainAuthorId());
					
					Draft draft = user.searchDraftById(purchasedList.get(i).getDraftId());
					
					purchasedDrafts.add(draft);
					
					purchasedAuthor.add(user);
				}
			}
			
			model.addAttribute("ep", ep);
			
			model.addAttribute("locale", locale);
			
			model.addAttribute("interestList",interestList);
			
			model.addAttribute("purchasedList",purchasedList);
			
			model.addAttribute("interestDrafts",interestDrafts);
			
			model.addAttribute("purchasedDrafts",purchasedDrafts);
			
			model.addAttribute("interestAuthor", interestAuthor);
			
			model.addAttribute("purchasedAuthor", purchasedAuthor);
			
			return "ephome";
		}
		else
		{
			return "redirect:/login";
		}
	}
	
	
	@RequestMapping(value = "/draftexchange", method = RequestMethod.GET)
	public String draftExchangeManagement(@RequestParam(value="action")String action, @RequestParam(value="author",required=false)String authorId, @RequestParam(value="draft",required=false)String draftId, @RequestParam(value="draftexchange",required=false)String draftExchangeId, Locale locale, Model model, HttpServletRequest req, HttpServletResponse response) {
		
		logger.info("Welcome draftExchangeManager! The client locale is {}.", locale);
		
		EntertainmentProfessional login_ep = (EntertainmentProfessional)req.getSession().getAttribute("login-ep");
		
		String logStr = "[DRAFTEXCHANGEMANAGER ACTION="+action+" & DRAFTEXCHANGE="+draftExchangeId+" & ENTERTAINMENTPROFESSIONAL="+login_ep.getId()+"] ";
		
		if(action.equals("add"))
		{
			DraftExchangeManagementService dems = new DraftExchangeManagementService(userMongoTemplate,easyDraftDBFactory);
			
			String result = dems.createNewDraftExchange(login_ep.getId(), authorId, draftId);
			
			if(result == null) 
				return "redirect:/ephome?result=failed";
			else if(result.equals("existed"))
				return "redirect:/ephome?result=existed";
			else
				return "redirect:/ephome?result=ok";
		}else if(action.equals("buy"))
		{
			DraftExchangeManagementService dems = new DraftExchangeManagementService(userMongoTemplate,easyDraftDBFactory);
			
			dems.buyDraftExchange(login_ep.getId(), draftExchangeId);
			
			return "redirect:/ephome";
		}else if (action.equals("delete"))
		{
			DraftExchangeManagementService dems = new DraftExchangeManagementService(userMongoTemplate,easyDraftDBFactory);
			
			dems.removeDraftExchange(login_ep.getId(), draftExchangeId);
			
			return "redirect:/ephome";
		}else if(action.equals("increaseinterestlevel"))
		{
			DraftExchangeManagementService dems = new DraftExchangeManagementService(userMongoTemplate,easyDraftDBFactory);
			
			dems.changeInterestLevel(login_ep.getId(), draftExchangeId, true);
			
			return "redirect:/ephome";
			
		}else if(action.equals("decreaseinterestlevel"))
		{
			DraftExchangeManagementService dems = new DraftExchangeManagementService(userMongoTemplate,easyDraftDBFactory);
			
			dems.changeInterestLevel(login_ep.getId(), draftExchangeId, false);
			
			return "redirect:/ephome";
		}else
			
			return null;
		
	}
	
	
}
