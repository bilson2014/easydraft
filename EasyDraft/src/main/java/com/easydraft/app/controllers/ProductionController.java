package com.easydraft.app.controllers;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.easydraft.app.beans.Draft;
import com.easydraft.app.beans.DraftPreview;
import com.easydraft.app.beans.DraftProduction;
import com.easydraft.app.beans.User;
import com.easydraft.app.services.DraftManagementService;
import com.easydraft.app.services.ProductionService;
import com.easydraft.app.services.ScreenplaySearchingServiceImpl;
import com.easydraft.app.services.UserManagementService;
import com.easydraft.app.util.Encoding;
/**
 * Handles requests for the draftviewer and production page.
 */
/**
 * @author sunbin
 *
 */
@Controller
public class ProductionController {
	@Autowired
	MongoTemplate userMongoTemplate;
	@Autowired
	MongoDbFactory easyDraftDBFactory;
	@Autowired
	ScreenplaySearchingServiceImpl screenplaySearchingServiceImpl;
	
	private static final Logger logger = LoggerFactory.getLogger(ProductionController.class);
	
	@RequestMapping(value = "/productionize", method = RequestMethod.GET)
	public String productionize(@RequestParam(value="draft")String draft_id, @RequestParam(value="user", required=false)String user_id, Locale locale, Model model, HttpServletRequest req)
	{
		if(user_id==null) user_id = ((User) req.getSession().getAttribute("login-user")).getId();
		
		ProductionService ps = new ProductionService(userMongoTemplate,easyDraftDBFactory);
		
		DraftProduction productionInfo = ps.getProductionInformation(draft_id, user_id);
		
		model.addAttribute("genres", DraftProduction.Genres.values());
		
		model.addAttribute("production", productionInfo);
		
		model.addAttribute("previews", productionInfo.getAllPreviews());
		
		model.addAttribute("exposedPreview",productionInfo.searchPreviewById(productionInfo.getExposedPreviewId()));
		
		UserManagementService ums = new UserManagementService(userMongoTemplate);
		
		User currentUser = ums.findUserById(user_id);
		
		Draft currentDraft = currentUser.searchDraftById(draft_id);
		
		model.addAttribute("currentDraft", currentDraft);
		
		return "production";
	}
	
	@RequestMapping(value = "/productionize", method = RequestMethod.POST, params = "save")
	public String saveProductionInfo(@RequestParam(value="draft",required=false)String draft_id, @RequestParam(value="user",required=false)String user_id, @ModelAttribute("production")DraftProduction production, Locale locale, Model model, HttpServletRequest req)
	{
		if(user_id==null) user_id = ((User) req.getSession().getAttribute("login-user")).getId();
		
		ProductionService ps = new ProductionService(userMongoTemplate,easyDraftDBFactory);
		
		String pitch = Encoding.transEncodingFromPage(production.getPitch());
		
		String logline = Encoding.transEncodingFromPage(production.getLogline());
		
		DraftProduction.Genres genre = production.getGenre();
		
		//save production
		int result = ps.saveProductionInfo(draft_id, user_id, pitch, logline, genre);
		
		//index production
		if(result==0) //save successful
		{
			UserManagementService ums = new UserManagementService(userMongoTemplate);
			
			User user = ums.findUserById(user_id);
			
			Draft draft = user.searchDraftById(draft_id);
			
			screenplaySearchingServiceImpl.indexScreenplayProduction(draft.getId(), draft.getTitle(), user.getId(), user.getShowName(), user.getFirstName()+" "+user.getLastName(), logline, pitch, genre.toString(), draft.getExternalURL(), new Date(System.currentTimeMillis()));
		}
		
		logger.info("[PRODUCTIONIZE] Updated the draft "+ draft_id + "for user "+user_id+" with short pitch ["+pitch+"] and logline["+logline+"]");
		
		System.out.println("[PRODUCTIONIZE] Updated the draft "+ draft_id + "for user "+user_id+" with short pitch ["+pitch+"] and logline["+logline+"]");
		
		return "redirect:/productionize?draft="+draft_id;
		
	}
	
	@RequestMapping(value = "/previeweditor", method = RequestMethod.GET)
	public String editPreview(@RequestParam(value="action")String action, @RequestParam(value="draft")String draft_id, @RequestParam(value="user",required=false)String user_id, @RequestParam(value="preview", required=false)String preview_id, Locale locale, Model model, HttpServletRequest req, HttpServletResponse response)
	{
		if(user_id==null) user_id = ((User) req.getSession().getAttribute("login-user")).getId();
		
		ProductionService ps = new ProductionService(userMongoTemplate,easyDraftDBFactory);
		
		logger.info("Welcome drafteditor! The client locale is {}.", locale);
		
		String logStr = "[PREVIEWEDITOR ACTION="+action+" & DRAFT="+draft_id+"] ";
		
		if(action.equals("new") && (preview_id==null || preview_id.equals("")))
		{
			logger.info(logStr+"User <" + ((User) req.getSession().getAttribute("login-user")).getShowName()+">" + " created a preview for draft <"+draft_id+">");
		
			String new_preview_id = ps.createDraftPreview(draft_id, user_id);
			
			System.out.println(logStr+"Created preview "+new_preview_id+" for draft "+draft_id+" user "+user_id);
			
			logger.info(logStr+"Created preview "+new_preview_id+" for draft "+draft_id+" user "+user_id);
			
			return "redirect:/previeweditor?action=new&draft="+draft_id+"&preview="+new_preview_id;
		}
		else if(action.equals("delete"))
		{
			ps.deletePreview(draft_id, user_id, preview_id);
			
			System.out.println(logStr+"Deleted preview "+preview_id+" for draft "+draft_id+" user "+user_id);
			
			logger.info(logStr+"Deleted preview "+preview_id+" for draft "+draft_id+" user "+user_id);
			
			return "redirect:/productionize?draft="+draft_id;
		}
		else if(action.equals("view"))
		{
			ByteArrayOutputStream contentOutStream = new ByteArrayOutputStream();
			
			if(ps.viewPreviewResult(draft_id, user_id, preview_id, contentOutStream)==0)
			{
				String contents = Encoding.transEncodingFromDB(contentOutStream);
				
				System.out.println("Content of the preview result file ["+preview_id+"] is "+contents);
				
				UserManagementService ums = new UserManagementService(userMongoTemplate);
				
				User user = ums.findUserById(user_id);
				
				Draft draft = user.searchDraftById(draft_id);
				
				DraftProduction dp = draft.getProduction();
				
				DraftPreview preview = dp.searchPreviewById(preview_id);
				
				System.out.println(logStr+"View preview "+preview_id+" for draft"+draft_id+" user "+user_id+" (TITLE = "+draft.getTitle()+" ; URL = "+preview.getPreviewURL()+"; CONTENT = "+contents+")");
				
				logger.info(logStr+"View preview "+preview_id+" for draft"+draft_id+" user "+user_id+" (TITLE = "+draft.getTitle()+" ; URL = "+preview.getPreviewURL()+"; CONTENT = "+contents+")");
				
				response.setContentType("text/html;charset=UTF-8");
				response.setCharacterEncoding("UTF-8");
				
				String prefix = "<html><head><title>"+draft.getTitle()+"("+preview.getPreviewName()+")</title></head><body>";
				String suffix = "</body></html>";
				
				try {
					response.getWriter().print(prefix);
					response.getWriter().print(contents);
					response.getWriter().print(suffix);
					response.getWriter().flush();
				}catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			// else reading file error
				
			return null;
		}
		else if(action.equals("set"))
		{
			ps.setPreview(draft_id, user_id, preview_id);
			
			System.out.println(logStr+"Set preview "+preview_id+" for draft "+draft_id+" user "+user_id);
			
			logger.info(logStr+"Set preview "+preview_id+" for draft "+draft_id+" user "+user_id);
			
			return "redirect:/productionize?draft="+draft_id;
		}
		else //update
		{
			ByteArrayOutputStream contentOutStream = new ByteArrayOutputStream();
			
			if(ps.getPreviewWork(draft_id, user_id, preview_id, contentOutStream)==0)
			{
				String contents = Encoding.transEncodingFromDB(contentOutStream);
				
				System.out.println(logStr+"Info: Content of the preview result file ["+preview_id+"] is "+contents);
				
				if(contents==null || contents.equals("")) //first time edit
				{
					DraftManagementService dms = new DraftManagementService(userMongoTemplate,easyDraftDBFactory);
					
					UserManagementService ums = new UserManagementService(userMongoTemplate);
					
					dms.getExistingDraft(draft_id, ums.findUserById(user_id) , contentOutStream);
					
					contents = Encoding.transEncodingFromDB(contentOutStream);
					
					System.out.println(logStr+"Info: There is no last edited preview. Content of the draft file ["+draft_id+"] is "+contents);
				}
				
				model.addAttribute("previewEditData", contents);
				
				UserManagementService ums = new UserManagementService(userMongoTemplate);
				
				User user = ums.findUserById(user_id);
				
				Draft draft = user.searchDraftById(draft_id);
				
				DraftProduction dp = draft.getProduction();
				
				DraftPreview preview = dp.searchPreviewById(preview_id);
				
				model.addAttribute("previewName", preview.getPreviewName());
				
				System.out.println(logStr+"Edit preview "+preview_id+" for draft"+draft_id+" user "+user_id+" (TITLE = "+draft.getTitle()+" ; URL = "+preview.getPreviewURL()+"; CONTENT = "+contents+")");
				
				logger.info(logStr+"Edit preview "+preview_id+" for draft"+draft_id+" user "+user_id+" (TITLE = "+draft.getTitle()+" ; URL = "+preview.getPreviewURL()+"; CONTENT = "+contents+")");
				
				return "previeweditor";
			}
			
			//else reading file error
			return null;
		}
			
	}
	
	@RequestMapping(value = "/previeweditor", method = RequestMethod.POST, params = "save")
	public String savePreview(@RequestParam(value="draft")String draft_id, @RequestParam(value="user",required=false)String user_id, @RequestParam(value="preview")String preview_id, Locale locale, Model model, HttpServletRequest req)
	{
		
		String editedPreviewContentRaw = null;
		
		String previewNameRaw =null;

		String generatedPreviewContentRaw = null;
		
		if(!req.getContentType().equalsIgnoreCase("application/x-www-form-urlencoded"))//not form
		{
			String editedPreviewPage = null;
			
			try{
				StringBuilder sb = new StringBuilder();
		        BufferedReader bufferedReader = req.getReader();  
		        char[] buf  = new char[4*1024];
		        int len;
		        try {  
		            while((len = bufferedReader.read(buf))!=-1){  
		            	sb.append(buf);
		            }  
		        } catch (IOException e) {  
		            e.printStackTrace();  
		        } finally {  
		            try{  
		            	bufferedReader.close(); 
		            }catch(IOException e){  
		                e.printStackTrace();  
		            }  
		        }  
				
				editedPreviewPage = sb.toString();
				
			}catch(IOException e)
			{
				e.printStackTrace();
			}
		
			String editedPreviewPageDecode = Encoding.transEncodingFromPage(editedPreviewPage);
			
			System.out.println(editedPreviewPageDecode);
			
			Document doc = null;
			
			try{
				DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				
			    InputSource is = new InputSource();
			    
			    is.setCharacterStream(new StringReader(editedPreviewPageDecode));
			    
			    doc = db.parse(is);
		    
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			editedPreviewContentRaw = doc.getElementById("preview_content").getTextContent();
			
			previewNameRaw = doc.getElementById("preview_name").getTextContent();
			
		}else //form
		{
			/*
			Iterator<String> it = req.getParameterMap().keySet().iterator();
			String key=null;
			String value=null;
			String keyandValue=null;
			while(it.hasNext())
			{
				key=it.next();
				value= ((String[])req.getParameterMap().get(key))[0];
				
				keyandValue=key+"="+value;  
                System.out.println(key+"= "+value); 
			}*/
			
			editedPreviewContentRaw = req.getParameter("previewEdit");
			
			previewNameRaw= ((String[])req.getParameterMap().get("preview_name"))[0];
			
			generatedPreviewContentRaw = ((String[])req.getParameterMap().get("previewResult"))[0];
		}
		
		
		
		if(user_id==null) user_id = ((User) req.getSession().getAttribute("login-user")).getId();
		
		ProductionService ps = new ProductionService(userMongoTemplate,easyDraftDBFactory);
		

		String previewName = Encoding.transEncodingFromPage(previewNameRaw);
		
		String editedPreviewContent = Encoding.transEncodingFromPage(editedPreviewContentRaw);
		
		String generatedPreviewContent = Encoding.transEncodingFromPage(generatedPreviewContentRaw);
		
		
		System.out.println("previewName = "+ previewName);
		System.out.println("editedPreviewContent" + editedPreviewContent);
		System.out.println("generatedPreviewContent"+generatedPreviewContent);
		
		System.out.println("[PREVIEWEDITOR SAVE] save preview for draft " + draft_id +" user " + user_id + " preview " +  preview_id + " (name = "+previewName+" ; edit content = "+editedPreviewContent+" ; generated content = "+generatedPreviewContent+")");
		
		ps.savePreview(draft_id, user_id, preview_id, previewName, editedPreviewContent, generatedPreviewContent);
		
		return "redirect:/productionize?draft="+draft_id;
		
	}
	
	@RequestMapping(value = "/previeweditor", method = RequestMethod.POST, params = "close")
	public String closePreview(@RequestParam(value="action")String action, @RequestParam(value="draft")String draft_id, @RequestParam(value="user",required=false)String user_id, @RequestParam(value="preview")String preview_id, Locale locale, Model model, HttpServletRequest req)
	{
		
		if(action.equals("new"))
		{
			if(user_id==null) user_id = ((User) req.getSession().getAttribute("login-user")).getId();
			
			ProductionService ps = new ProductionService(userMongoTemplate,easyDraftDBFactory);
			
			ps.deletePreview(draft_id, user_id, preview_id);
			
			System.out.println("[PREVIEWEDITOR CLOSE] Deleted preview "+preview_id+" for draft "+draft_id+" user "+user_id);
			
			logger.info("[PREVIEWEDITOR CLOSE] Deleted preview "+preview_id+" for draft "+draft_id+" user "+user_id);
			
			return "redirect:/productionize?draft="+draft_id;
			
		}else
		{
			return "redirect:/productionize?draft="+draft_id;
		}
		
		
	}
}
