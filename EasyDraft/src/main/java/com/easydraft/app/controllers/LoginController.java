package com.easydraft.app.controllers;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.easydraft.app.beans.EntertainmentProfessional;
import com.easydraft.app.beans.User;
import com.easydraft.app.services.UserManagementService;
/**
 * Handles requests for the application login page.
 */
/**
 * @author sunbin
 *
 */
@Controller
public class LoginController {
	
	@Autowired
	MongoTemplate userMongoTemplate;

	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String defaultLogin(Locale locale, Model model, HttpServletRequest req) {
		
		logger.info("Welcome Register! The client locale is {}.", locale);
		
		User user = (User) req.getSession().getAttribute("login-user");
		
		if(user!=null)
		{	
			return "redirect:/home";
		}
		else
		{
			user = new User();
			
			model.addAttribute("user", user);
			
			return "login";
		}
		
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Locale locale, Model model) {
		
		logger.info("Welcome login! The client locale is {}.", locale);
				
		User user = new User();
		
		model.addAttribute("user", user);
		
		return "login";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String initialLoginAction (@ModelAttribute("user")User user, Locale locale, Model model, HttpServletRequest req) {
		
		UserManagementService ums = new UserManagementService(userMongoTemplate);
		
		String identity = req.getParameter("identity");
		
		System.out.println("Login as a "+identity);
		
		if(identity.equals("writer"))//writer login
		{
			User validated_user = ums.userLogin(user.getShowName(), user.getPassword());
			
			if(validated_user!=null)
			{	
				logger.info("Login successful - user "+user.getShowName());
				
				req.removeAttribute("login-ep");
				
				req.getSession().setAttribute("login-user", validated_user);
				
				return "redirect:/home";
			}
			else
			{
				logger.info("Login failed - invalid user name or password - user "+user.getShowName());
				
				model.addAttribute("alert", "The user does not exist, please check the user name and password.");
				
				return "login";
			}
		}else
		{
			EntertainmentProfessional validated_ep = ums.entertainmentProfessionalLogin(user.getShowName(), user.getPassword());
			
			if(validated_ep!=null)
			{
				logger.info("Login successful - Entertainment Professional "+user.getShowName());
				
				req.removeAttribute("login-user");
				
				req.getSession().setAttribute("login-ep", validated_ep);
				
				return "redirect:/ephome";
			}else
			{
				logger.info("Login failed - invalid user name or password - user "+user.getShowName());
				
				model.addAttribute("alert", "The entertainment professional does not exist, please check the user name and password.");
				
				return "login";
			}
		}
	}
	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginAction (@ModelAttribute("user")User user, Locale locale, Model model, HttpServletRequest req) {
		
		UserManagementService ums = new UserManagementService(userMongoTemplate);
		
		String identity = req.getParameter("identity");
		
		System.out.println("Login as a "+identity);
		
		if(identity.equals("writer"))//writer login
		{
			User validated_user = ums.userLogin(user.getShowName(), user.getPassword());
			
			if(validated_user!=null)
			{	
				logger.info("Login successful - user "+user.getShowName());
				
				req.removeAttribute("login-ep");
				
				req.getSession().setAttribute("login-user", validated_user);
				
				return "redirect:/home";
			}
			else
			{
				logger.info("Login failed - invalid user name or password - user "+user.getShowName());
				
				model.addAttribute("alert", "The user does not exist, please check the user name and password.");
				
				return "login";
			}
		}else
		{
			EntertainmentProfessional validated_ep = ums.entertainmentProfessionalLogin(user.getShowName(), user.getPassword());
			
			if(validated_ep!=null)
			{
				logger.info("Login successful - Entertainment Professional "+user.getShowName());
				
				req.removeAttribute("login-user");
				
				req.getSession().setAttribute("login-ep", validated_ep);
				
				return "redirect:/ephome";
			}else
			{
				logger.info("Login failed - invalid user name or password - user "+user.getShowName());
				
				model.addAttribute("alert", "The entertainment professional does not exist, please check the user name and password.");
				
				return "login";
			}
		}
	}
	
	
}
