package com.easydraft.app.util;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

/**
 * @author sunbin
 *
 */
public class Encoding {
	
	public static String transEncodingFromPage(String in){
		
		try{
			in = new String(in.getBytes("ISO-8859-1"),"UTF-8");
		}catch(UnsupportedEncodingException e){
			e.printStackTrace();
		}
		
		return in;
	}
	
	public static String transEncodingFromDB(ByteArrayOutputStream baoutstream)
	{
		String out = null;
		
		try{
			
			out = new String(baoutstream.toByteArray(),"UTF-8");
			
		}catch(UnsupportedEncodingException e)
		{
			e.printStackTrace();
		}
		
		return out;
	}
	

}
